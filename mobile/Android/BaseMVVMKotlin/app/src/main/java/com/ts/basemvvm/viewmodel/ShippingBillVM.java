package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.ts.basemvvm.api.BiddingApi;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.model.BiddingInformation;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShippingBillVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<BiddingInformation> mInformationBidding;
    public int mStatus = 0;

    public ShippingBillVM(@NonNull Application application) {
        super(application);
        mInformationBidding = new ArrayList<>();
    }


    public void requestListBiddingOrderShipping(String txt_search, int offset, int status, RunUi runUi) {
        isLoading.set(true);
        BiddingApi.getListBiddingOrderShipping(txt_search, offset, status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);

                if (o == null) {
                    runUi.run(Constants.FAIL_API);
                    return;
                }

                mInformationBidding.addAll((ArrayList)o);

                runUi.run(Constants.SUCCESS_API);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void clearData() {
        mInformationBidding.clear();
    }
}
