package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.luck.picture.lib.entity.LocalMedia;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancelOrderDialogVM extends BaseViewModel {
    private List<LocalMedia> lstFileSelected;
    private List<String> listImage=new ArrayList<>();
    private ObservableBoolean isNotEmptyImage = new ObservableBoolean(false);
    public CancelOrderDialogVM(@NonNull Application application) {
        super(application);
        isNotEmptyImage.set(false);
    }
    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }
}
