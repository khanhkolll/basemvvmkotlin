package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.basemvvm.api.CustomerApi;
import com.ts.basemvvm.api.DriverApi;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.model.Driver;
import com.ts.basemvvm.model.DriverReward;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MeVM extends BaseViewModel {
    ObservableField<DriverReward> driver_reward = new ObservableField<>();
    ObservableField<String> ratingStar = new ObservableField<>();
    ObservableField<Float> pointStar = new ObservableField<>();
    ObservableField<Driver> driverObservable = new ObservableField<>();


    public MeVM(@NonNull Application application) {
        super(application);
        getDriverReward();
    }

    public void getDriverReward() {
        DriverApi.getDriverReward(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                driver_reward.set((DriverReward) o);
                handleData(driver_reward.get());
            }

            @Override
            public void onFail(Throwable error) {

            }

        });
    }

    public void handleData(DriverReward driverReward) {
        List<Integer> rates = driverReward.getNum_rating();

        int[] raters = new int[]{
                rates.get(0),
                rates.get(1),
                rates.get(2),
                rates.get(3),
                rates.get(4)
        };
        Float rating_star = RatingStar(raters);
        if (rating_star > 0) {
            ratingStar.set(AppController.getInstance().formatNumber(rating_star));
            pointStar.set(rating_star);
        } else {
            ratingStar.set("5");
            pointStar.set((float) 5);
        }
    }

    private Float RatingStar(int[] rates) {
        int total_rating = 0;
        int quantity_rating = 0;
        for (int i = 0; i < rates.length; i++) {
            total_rating += rates[i] * (5 - i);
            quantity_rating += rates[i];
        }
        Float result = Float.valueOf(total_rating) / quantity_rating;
        return result;
    }

    public void logout() {
        DriverApi.logout();
    }

    public void getUserInfo(RunUi runUi) {

        CustomerApi.getDriver(new IResponse<OdooResultDto<Driver>>() {
            @Override
            public void onSuccess(OdooResultDto<Driver> o) {
                if (o != null && o.getRecords().size() > 0) {
                    Driver driver = o.getRecords().get(0);
                    StaticData.setDriver(driver);
                    driverObservable.set(driver);
                    runUi.run("Get Info Driver Success");
                }
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }
}
