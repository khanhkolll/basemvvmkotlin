package com.ts.basemvvm.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area extends BaseModel {
    private Integer id;
    private String name;
    private String code;
}
