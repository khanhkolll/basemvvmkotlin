package com.ts.basemvvm.socket_notification;

public interface SocketIONavigation {
    void connect(String uri, String channel, SocketIO.onMessageSocket onMessageSocket);

    void disconnect(String channel);
}
