package com.ts.basemvvm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.BaseAdapterV3;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.ShippingBillFragmentBinding;
import com.ts.basemvvm.enums.EnumBidding;
import com.ts.basemvvm.model.BiddingInformation;
import com.ts.basemvvm.ui.activity.DetailNotShipActivity;
import com.ts.basemvvm.viewmodel.ShippingBillVM;
import com.ts.basemvvm.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ShippingBillFragment extends BaseFragment {
    private int mKeyPage;
    private ShippingBillFragmentBinding mBinding;
    private ShippingBillVM mShippingBillVM;
    private BaseAdapterV3 mShippingBillAdapter;
    private int mOffset = 0;
    private String txtSearch = "";
    private DialogConfirm dialog;

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (ShippingBillFragmentBinding) binding;
        mShippingBillVM = (ShippingBillVM) viewModel;
        getData();
        initView();
        initLoadMore();
        initRefreshData();
        eventChangeEdtSearch();
        return view;
    }

    private void initView() {
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mShippingBillAdapter = new BaseAdapterV3(R.layout.item_bid, mShippingBillVM.getMInformationBidding(), this) {
            @Override
            protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
                ((BiddingInformation) o).setBiddingOrderStatus(mShippingBillVM.mStatus);
                super.convert(holder, o);

            }
        };
        mBinding.recyclerView.setAdapter(mShippingBillAdapter);
    }

    /**
     * khởi tạo load more adapter
     */
    private void initLoadMore() {
        mShippingBillAdapter.getLoadMoreModule().setOnLoadMoreListener(this::loadMore);
        mShippingBillAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        mShippingBillAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void initRefreshData() {
        mBinding.swipeLayout.setOnRefreshListener(() -> refreshData());
    }


    private void getData() {
        assert getArguments() != null;
        mKeyPage = getArguments().getInt(Constants.KEY_PAGE);
        switch (mKeyPage) {
            case EnumBidding.EnumBiddingBehavior.NOT_SHIPPING:
                mShippingBillVM.mStatus = 0;
                break;
            case EnumBidding.EnumBiddingBehavior.SHIPPING:
                mShippingBillVM.mStatus = 1;
                break;
            case EnumBidding.EnumBiddingBehavior.SHIPPING_DONE:
                mShippingBillVM.mStatus = 2;
                break;
        }
        requestBiddingShipping(txtSearch, mOffset, mShippingBillVM.mStatus);
    }

    private void requestBiddingShipping(String txt_search, int offset, int status) {
        mShippingBillVM.requestListBiddingOrderShipping(txt_search, offset, status, this::runUi);
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            mOffset++;
            mShippingBillAdapter.notifyDataSetChanged();
            mBinding.recyclerView.invalidate();
            offRefreshing();
            emptyData(mShippingBillVM.getMInformationBidding());
        }
        if (action.equals(Constants.FAIL_API)) {
            offRefreshing();
            emptyData(mShippingBillVM.getMInformationBidding());

        }
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.viewGroup:
            case R.id.viewScrCargo:
                BiddingInformation biddingShipping = (BiddingInformation) o;
                Bundle args = new Bundle();
                Intent intent = new Intent(getActivity(), DetailNotShipActivity.class);
                intent.putExtra(Constants.DATA_PASS_FRAGMENT, args);
                intent.putExtra(Constants.KEY_PAGE, mShippingBillVM.mStatus);
                intent.putExtra(Constants.OBJECT_SERIALIZABLE, biddingShipping.getId());
                startActivityForResult(intent, Constants.REQUEST_UPDATE);
                break;
        }
    }

    private void offRefreshing() {
        mBinding.swipeLayout.setRefreshing(false);
        mShippingBillAdapter.getLoadMoreModule().setEnableLoadMore(true);
    }

    private void refreshData() {
        // refresh data
        mShippingBillVM.clearData();
        mOffset = 0;
        requestBiddingShipping(txtSearch, mOffset, mShippingBillVM.mStatus);
        mShippingBillAdapter.notifyDataSetChanged();
    }

    private void loadMore() {
        int length = mShippingBillVM.getMInformationBidding().size();
        int limit = 10;
        if (length == limit) {
            requestBiddingShipping(txtSearch, mOffset, mShippingBillVM.mStatus);
        } else {
            //thông báo không còn gì để load
            mShippingBillAdapter.getLoadMoreModule().loadMoreEnd();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_UPDATE) {
            if (resultCode == RESULT_OK) {
                String returnedResult = data.getDataString();
                if (returnedResult != null) {
                    refreshData();
                }
                // OR
                // String returnedResult = data.getDataString();
            }
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        super.action(view, baseViewModel);
        switch (view.getId()) {
            case R.id.btnSearch:
                if (mBinding.edtSearch.getText().toString().trim().isEmpty())
                    return;
                txtSearch = mBinding.edtSearch.getText().toString().trim();
                refreshData();
                break;
        }
    }

    private void eventChangeEdtSearch() {
        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    txtSearch = "";
                    refreshData();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void emptyData(List<?> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            mBinding.emptyData.setVisibility(View.GONE);
            mBinding.recyclerView.setVisibility(View.VISIBLE);
        } else {
            mBinding.emptyData.setVisibility(View.VISIBLE);
            mBinding.recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.shipping_bill_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ShippingBillVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
