package com.ts.basemvvm.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.StatisticalChildFragmentBinding;
import com.ts.basemvvm.model.StatisticalWalletDTO;
import com.ts.basemvvm.viewmodel.StatisticalChildVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StatisticalChildFragment extends BaseFragment {
    StatisticalChildVM statisticalChildVM;
    StatisticalChildFragmentBinding mBinding;
    int emptyData = 0;
    int month;
    int year;
    StatisticalWalletDTO statisticalWalletDTO;

    public StatisticalChildFragment(Integer month,Integer year,StatisticalWalletDTO statisticalWalletDTO) {
        this.month =month;
        this.year=year;
        this.statisticalWalletDTO = statisticalWalletDTO;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        statisticalChildVM = (StatisticalChildVM) viewModel;
        mBinding = (StatisticalChildFragmentBinding) binding;
        initView();

        return view;
    }

    private void initView() {

        mBinding.rcReceiveMoney.setMax(100);
        mBinding.rcUseMoney.setMax(100);
        if (statisticalWalletDTO.getTotal_pay() == null) {
            emptyData++;
            statisticalWalletDTO.setTotal_pay((float) 0);
        }
        if (statisticalWalletDTO.getTotal_save() == null) {
            emptyData++;
            statisticalWalletDTO.setTotal_save((float) 0);
        }
        if (emptyData == 2) {
            statisticalChildVM.getEmptyData().set(true);
        } else {
            statisticalChildVM.getEmptyData().set(false);
        }
        Float total = statisticalWalletDTO.getTotal_pay() + statisticalWalletDTO.getTotal_save();
        Float pay = (statisticalWalletDTO.getTotal_pay() / total) * 100;
        Float save = 100 - pay;
        mBinding.rcReceiveMoney.setProgress(save);
        mBinding.rcUseMoney.setProgress(pay);
        statisticalWalletDTO.getTotal_pay();
        mBinding.txtReceiveMoney.setText(statisticalWalletDTO.getTotal_saveStr() +"(" + String.format("%.1f", save) + "%)");
        mBinding.txtUseMoney.setText(statisticalWalletDTO.getTotal_payStr()+ "(" + String.format("%.1f", pay) + "%)");
        mBinding.txtTotalMoney.setText(AppController.getInstance().formatCurrency(total) + "(100%)");


        mBinding.chartPie.setUsePercentValues(true);
        mBinding.chartPie.getDescription().setEnabled(false);
        mBinding.chartPie.setExtraOffsets(5, 10, 5, 5);

        mBinding.chartPie.setDragDecelerationFrictionCoef(0.95f);


        mBinding.chartPie.setTransparentCircleColor(Color.WHITE);
        mBinding.chartPie.setTransparentCircleAlpha(110);

        mBinding.chartPie.setDrawCenterText(true);

        mBinding.chartPie.setRotationAngle(0);
        // enable rotation of the chart by touch


        mBinding.chartPie.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = mBinding.chartPie.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        mBinding.chartPie.setEntryLabelColor(Color.WHITE);
        mBinding.chartPie.setEntryLabelTextSize(12f);
        setData(2, statisticalWalletDTO.getTotal_pay(), statisticalWalletDTO.getTotal_save());
    }

    private void setData(int count, float range1, float range2) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(new PieEntry(range1,
                getString(R.string.total_expenditure),
                getResources().getDrawable(R.drawable.ic_cup_gold)));
        entries.add(new PieEntry(range2,
                getString(R.string.total_revenue),
                getResources().getDrawable(R.drawable.ic_cup_gold)));

        PieDataSet dataSet = new PieDataSet(entries, getString(R.string.note));

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.accent));
        colors.add(getResources().getColor(R.color.demo_state_foreground_color));

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        mBinding.chartPie.setData(data);

        // undo all highlights
        mBinding.chartPie.highlightValues(null);

        mBinding.chartPie.invalidate();
    }
    public void gotoWalletChildFragment(String receive_type){

        Intent intent= new Intent(getContext(), CommonActivity.class);
        Bundle bundle= new Bundle();
        bundle.putString("receive_type",receive_type);
        bundle.putBoolean(Constants.ONLY_READ,true);
        bundle.putSerializable(Constants.DATA_PASS_FRAGMENT,statisticalWalletDTO);
        bundle.putInt("month",month);
        bundle.putInt("year",year);
        bundle.putSerializable(Constants.FRAGMENT,WalletChildFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    @Override
    public int getLayoutRes() {
        return R.layout.statistical_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return StatisticalChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }
}
