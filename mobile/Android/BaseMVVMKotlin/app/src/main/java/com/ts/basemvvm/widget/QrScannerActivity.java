/*
 * Copyright (C) 2018 Jenly Yu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ts.basemvvm.widget;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.king.zxing.CaptureActivity;
import com.king.zxing.camera.FrontLightMode;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.model.BillPackage;
import com.ts.basemvvm.model.ShareVanRoutingPlan;
import com.ts.basemvvm.ui.fragment.BillPackageDialog;
import com.ts.basemvvm.utils.StatusBarUtils;
import com.ts.basemvvm.utils.ToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Jenly <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
public class QrScannerActivity extends CaptureActivity implements View.OnClickListener {

    // Multi scan qrCode
    private String FROM_ACTIVITY;
    private boolean isMultiScanQrCode;// kiểm tra có phải quét multi qr không
    private String qrResult; // qr code quét được
    TextView tvQuantityCheck; // tv hiển thị số lượng quét


    //Dữ liệu từ màn hình RoutingDetailActivity
    private static final String FROM_ROUTING_DETAIL_ACTIVITY = "FROM_ROUTING_DETAIL_ACTIVITY";
    private HashMap<Integer, HashMap<String, Boolean>> qrDataChecked = new HashMap<>();
    private ShareVanRoutingPlan routingPlan = new ShareVanRoutingPlan();
    private final List<BillPackage> listBillPackage = new ArrayList<>();
    private int index = 0;
    BillPackageDialog dialogFragment;
    boolean checkScan = true;


    @Override
    public int getLayoutId() {
        return R.layout.easy_capture_activity;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Toolbar toolbar = findViewById(R.id.toolbar);
        StatusBarUtils.immersiveStatusBar(this, toolbar, 0.2f);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvQuantityCheck = findViewById(R.id.tvQuantityCheck);
        tvTitle.setText(getIntent().getStringExtra("QR_SCANNER"));
        getData();
        if (isMultiScanQrCode && FROM_ROUTING_DETAIL_ACTIVITY.equals(FROM_ACTIVITY)) {
            tvQuantityCheck.setVisibility(View.VISIBLE);
            tvQuantityCheck.setText(routingPlan.getTotal_qr_checked() + "/" + routingPlan.getTotal_qr_check());
        }
        getCaptureHelper()
//                .decodeFormats(DecodeFormatManager.QR_CODE_FORMATS)
                .playBeep(true)
                .vibrate(true)
                .frontLightMode(FrontLightMode.AUTO)
                .tooDarkLux(45f)
                .brightEnoughLux(100f)
                .continuousScan(isMultiScanQrCode)
                .supportLuminanceInvert(true);
        findViewById(R.id.ivLeft).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivLeft) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if (isMultiScanQrCode) {
            setDataResult(false);
        }
        super.onBackPressed();
    }

    // Lấy dữ liệu từ màn hình khác sang
    public void getData() {
        Intent intent = this.getIntent();
        if (intent.hasExtra(Constants.MULTI_SCAN_QR_CODE)) {
            isMultiScanQrCode = intent.getBooleanExtra(Constants.MULTI_SCAN_QR_CODE, false);
            if (isMultiScanQrCode) {
                FROM_ACTIVITY = intent.getStringExtra(Constants.FROM_ACTIVITY);
                if (FROM_ROUTING_DETAIL_ACTIVITY.equals(FROM_ACTIVITY)) {
                    qrDataChecked = (HashMap<Integer, HashMap<String, Boolean>>) intent.getSerializableExtra(Constants.QR_DATA);
                    routingPlan = (ShareVanRoutingPlan) intent.getSerializableExtra(Constants.DATA);
                    listBillPackage.addAll(routingPlan.getList_bill_package());
                }
            }
        }
    }

    @Override
    public boolean onResultCallback(String result) {
        if (isMultiScanQrCode) {
            if (FROM_ROUTING_DETAIL_ACTIVITY.equals(FROM_ACTIVITY) && checkScan) {
                checkScan = false;
                qrResult = result;
                sleepScan();
            }
        }
        return super.onResultCallback(result);
    }

    private void sleepScan() {
        getCaptureHelper().vibrate(false);
        int checkQrCode = handleResult();
        if (checkQrCode == 0) {
            showDialog();
        } else if (checkQrCode == 1) {
            showToast(R.string.QrCode_has_been_scanned, R.drawable.ic_danger);
        } else if (checkQrCode == 2) {
            if (FROM_ACTIVITY.equals(FROM_ROUTING_DETAIL_ACTIVITY)) {
                showToast(getString(R.string.qr_code) + " " + qrResult + " " + getString(R.string.is_not_correct), R.drawable.ic_close14);
            }
        } else {
            setDataResult(true);
            showToast(R.string.Complete, R.drawable.ic_check);
            finish();
        }
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                getCaptureHelper().vibrate(true);
                checkScan = true;
            }
        }.start();
    }

    private void showDialog() {
        if (FROM_ACTIVITY.equals(FROM_ROUTING_DETAIL_ACTIVITY)) {
            tvQuantityCheck.setText(routingPlan.getTotal_qr_checked() + "/" + routingPlan.getTotal_qr_check());// set text number qr code checked
            FragmentManager fm = getSupportFragmentManager();
            dialogFragment = new BillPackageDialog(listBillPackage.get(index), routingPlan.getType(), qrResult);
            dialogFragment.show(fm, "ABC");
        }
    }

    private void showToast(Integer toast, Integer icon) {
        Log.d("Show_toast", getResources().getString(toast));
        this.runOnUiThread(new Runnable() {
            public void run() {
                ToastUtils.showToast(QrScannerActivity.this, getResources().getString(toast), getResources().getDrawable(icon));
            }
        });
    }

    private void showToast(String toast, Integer icon) {
        Log.d("Show_toast", toast);
        this.runOnUiThread(new Runnable() {
            public void run() {
                ToastUtils.showToast(QrScannerActivity.this, toast, getResources().getDrawable(icon));
            }
        });
    }


    // 0 Chưa quét , 1 Đã quét , 2 Sai mã ,3 Hoàn thành
    private Integer handleResult() {
        int index = 0;
        switch (FROM_ACTIVITY) {
            case FROM_ROUTING_DETAIL_ACTIVITY:
                for(int i=0;i<listBillPackage.size();i++){
                    BillPackage item = listBillPackage.get(i);
                    Boolean check = qrDataChecked.get(item.getBill_package_id()).get(qrResult);
                    if (check != null) {
                        if (check) {
                            return 1;
                        } else {
                            qrDataChecked.get(item.getBill_package_id()).put(qrResult, true);
                            item.setQuantityQrChecked(item.getQuantityQrChecked() + 1);
                            if (checkComplete()) {
                                return 3;
                            }
                            this.index = index;
                            routingPlan.setTotal_qr_checked(routingPlan.getTotal_qr_checked() + 1);
                            return 0;
                        }
                    }
                    index++;
                }
                return 2;
            default:
                return 2;
        }
    }

    private boolean checkComplete() {
        boolean check=true;
        switch (FROM_ACTIVITY) {
            case FROM_ROUTING_DETAIL_ACTIVITY:

//                for(int i=0;i<listBillPackage.size();i++){
                for(BillPackage item:listBillPackage){
//                    BillPackage item=listBillPackage.get(i);
                    if (item.getQuantity_import().equals(item.getQuantityQrChecked())) {
                        item.setQrChecked(true);
                    }else{
                        check=false;
                    }
                }
                break;

        }
        return check;
    }

    private void setDataResult(Boolean complete) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        if (FROM_ROUTING_DETAIL_ACTIVITY.equals(FROM_ACTIVITY)) {
            routingPlan.getList_bill_package().clear();
            routingPlan.getList_bill_package().addAll(listBillPackage);
            bundle.putBoolean(Constants.COMPLETE, complete);
            bundle.putSerializable(Constants.DATA, routingPlan);
            intent.putExtra(Constants.QR_DATA, qrDataChecked);
            intent.putExtras(bundle);
            setResult(Constants.RESULT_OK, intent);
        }
    }

}
