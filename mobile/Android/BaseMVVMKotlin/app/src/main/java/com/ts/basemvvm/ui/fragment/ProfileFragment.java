package com.ts.basemvvm.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.taufiqrahman.reviewratings.BarLabels;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.XBaseAdapter;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.ProfileFragmentBinding;
import com.ts.basemvvm.enums.ClickAction;
import com.ts.basemvvm.enums.ObjectStatus;
import com.ts.basemvvm.model.NotificationModel;
import com.ts.basemvvm.service.notification_service.NotificationService;
import com.ts.basemvvm.utils.ZoomImage;
import com.ts.basemvvm.viewmodel.ProfileVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ProfileFragment extends BaseFragment {
    private ProfileFragmentBinding mBinding;
    private ProfileVM profileVM;
    private XBaseAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (ProfileFragmentBinding) binding;
        profileVM = (ProfileVM) viewModel;
        if (StaticData.getDriver().getExpires_date() != null && checkExpiresDate()) {
//            mBinding.lbExpiresDate.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_danger),null, null,  null);
            mBinding.txtExpiresDate.setTextColor(getResources().getColor(R.color.color_price));
        }
        initView();
        profileVM.getDriverReward(this::runUi);
        profileVM.getDriverObservable().set(StaticData.getDriver());
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Log.i(ProfileFragment.class.getName(), "onStart: ...");
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.i(ProfileFragment.class.getName(), "onStop: ...");
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {
        Gson gsonn = new GsonBuilder()
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        NotificationModel notificationModell = gsonn.fromJson(notificationService.getMess_object(), NotificationModel.class);
        if (ObjectStatus.FOUR.equals(notificationModell.getObject_status()) && notificationModell.getClick_action().equals(ClickAction.MAIN_ACTIVITY)) {
            profileVM.getUserInfo(this::runUi);
        }
    }

    public boolean checkExpiresDate() {
        if (StaticData.getDriver().getExpires_date().getTime() < (new Date()).getTime()) {
            return true;
        }
        return false;
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if ("getRewardSuccess".equals(action)) {
            adapter = new XBaseAdapter(R.layout.item_armorial, profileVM.getDriverReward().get().getBadges(), this);
            mBinding.rcArmorial.setAdapter(adapter);

            List<Integer> rates = profileVM.getDriverReward().get().getNum_rating();

            int[] raters = new int[]{
                    rates.get(0),
                    rates.get(1),
                    rates.get(2),
                    rates.get(3),
                    rates.get(4)
            };
            Float rating_star = RatingStar(raters);
            if (rating_star > 0) {
                mBinding.txtRating.setText(AppController.getInstance().formatNumber(rating_star));
            } else {
                mBinding.txtRating.setText("0");
            }
            mBinding.rating.setRating(rating_star);
            mBinding.ratingReviews.createRatingBars(100, BarLabels.STYPE1, Color.parseColor("#F6AF36"), raters);
        }
    }

    private Float RatingStar(int[] rates) {
        int total_rating = 0;
        int quantity_rating = 0;
        for (int i = 0; i < rates.length; i++) {
            total_rating += rates[i] * (5 - i);
            quantity_rating += rates[i];
        }
        Float result = Float.valueOf(total_rating) / quantity_rating;
        return result;
    }

    private void initView() {
        mBinding.toolbar.setTitle(R.string.profile_info);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        mBinding.rcArmorial.setLayoutManager(layoutManager);

    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.ctReWard) {
            openRewardDetail();
        } else if (v.getId() == R.id.imgDriver) {
            zoomAvatar();
        }
    }

    public void zoomAvatar() {
        ZoomImage.zoom(this, StaticData.getDriver().getImage_1920());
    }

    public void openRewardDetail() {

        Intent intent = new Intent(requireActivity(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, DriverRewardFragment.class);
        requireActivity().startActivity(intent);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.profile_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProfileVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
