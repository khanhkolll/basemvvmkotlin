package com.ts.basemvvm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.XBaseAdapter;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.RatingCustomerFragmentBinding;
import com.ts.basemvvm.model.RatingBadges;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.RatingCustomerVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

public class RatingCustomerFragment extends BaseFragment {
    XBaseAdapter adapter;
    RatingCustomerFragmentBinding mBinding;
    RatingCustomerVM ratingCustomerVM;
    int routing_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (RatingCustomerFragmentBinding) binding;
        ratingCustomerVM = (RatingCustomerVM) viewModel;
        initView();
        setUpRecycleView();
        getData();
        return view;
    }

    private void initView() {
        mBinding.toolbar.setTitle(R.string.rating_customer_title);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mBinding.rbRatingStar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Integer number_star = Math.round(ratingBar.getRating());
                ratingCustomerVM.getRatingBadges().clear();
                if (ratingCustomerVM.getHashMapRatingBadges().get(number_star + "") != null) {
                    List<RatingBadges> list = ratingCustomerVM.getHashMapRatingBadges().get(number_star + "");
                    //Clear state check RatingBadges when select again star
                    for (RatingBadges ratingBadges : list) {
                        ratingBadges.setCheck(false);
                    }
                    ratingCustomerVM.getRatingBadges().addAll(list);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnSave) {
            mBinding.toolbar.setEnabled(false);
            ratingCustomerVM.getIsBtnRating().set(false);
            Integer number_star = Math.round(mBinding.rbRatingStar.getRating());
            ratingCustomerVM.handleData(routing_id, number_star, this::runUi);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(Constants.ROUTING_ID)) {
            routing_id = intent.getIntExtra(Constants.ROUTING_ID, 0);
            ratingCustomerVM.getCustomerInfo(routing_id, this::runUi);
        }
        ratingCustomerVM.getDriverRatingBadges(this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                ratingCustomerVM.getIsLoading().set(false);
                adapter.notifyDataSetChanged();
                break;
            case Constants.RATING_SUCCESS:
                ToastUtils.showToast(getActivity(), getString(R.string.rating_customer_success), getResources().getDrawable(R.drawable.ic_check));
                setResult();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                    }
//                },3000);
                break;
            case Constants.RATING_FAIL:
                mBinding.toolbar.setEnabled(true);
                ratingCustomerVM.getIsBtnRating().set(true);
                ToastUtils.showToast(getActivity(), getString(R.string.rating_customer_fail), getResources().getDrawable(R.drawable.ic_danger));
                break;
            case Constants.FAIL_API:
                break;
        }
    }

    private void setResult() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        Integer number_star = Math.round(mBinding.rbRatingStar.getRating());
        bundle.putInt(Constants.DATA_RESULT, number_star);
        intent.putExtras(bundle);
        getBaseActivity().setResult(Constants.RESULT_OK, intent);
        getActivity().onBackPressed();
    }


    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.rating_customer_item, ratingCustomerVM.getRatingBadges(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((RatingBadges) o).index - 1;
                if (!ratingCustomerVM.getRatingBadges().get(position).isCheck()) {
                    ratingCustomerVM.getRatingBadges().get(position).setCheck(true);
                } else {
                    ratingCustomerVM.getRatingBadges().get(position).setCheck(false);
                }
                adapter.notifyItemChanged(position);

            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcRateBadges.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false);
        mBinding.rcRateBadges.setLayoutManager(layoutManager);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.rating_customer_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RatingCustomerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
