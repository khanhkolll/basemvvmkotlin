package com.ts.basemvvm.model;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmBiddingResponse extends BaseModel {
    private boolean result;
    private List<Integer> list_cargo_invalid;
}
