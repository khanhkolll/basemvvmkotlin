package com.ts.basemvvm.api;

import com.luck.picture.lib.entity.LocalMedia;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.SOService;
import com.ts.basemvvm.model.ApiResponseModel;
import com.ts.basemvvm.model.Partner;
import com.ts.basemvvm.model.RatingBadges;
import com.ts.basemvvm.model.RatingCustomerDTO;
import com.ts.basemvvm.model.ShareVanRoutingPlan;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutingApi extends BaseApi {
    private static final String ROUTE_ROUTING = "/share_van_order/routing_plan_day";
    private static final String ROUTE_ROUTING_DETAIL = "/share_van_order/routing_plan_day/detail";
    private static final String ROUTE_LIST_RATING_BADGES = "/share_van_order/get_list_rating_badges";
    private static final String ROUTE_GET_CUSTOMER_INFO = "/share_van_order/get_rating_customer_info";
    private static final String ROUTE_RATING_CUSTOMER = "/share_van_order/rating_customer";

    public static void cancelRoutingPlanDay(String description_cancel, List<LocalMedia> listImageCancel, Integer bill_lading_detail_id, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        MultipartBody.Part[] parts = null;
        if (listImageCancel != null) {
            parts = new MultipartBody.Part[listImageCancel.size()];
            for (int i = 0; i < listImageCancel.size(); i++) {
                File file = new File(listImageCancel.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }
        Call<ResponseBody> call = soService.cancelRoutingPlanDay(mOdoo.getSessionCokie(), bill_lading_detail_id, description_cancel, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void notContactCustomer(List<String> listImageCancel, List<Integer> routing_plan_day_id, Integer type, String time, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        MultipartBody.Part[] parts = null;
        if (listImageCancel != null) {
            parts = new MultipartBody.Part[listImageCancel.size()];
            for (int i = 0; i < listImageCancel.size(); i++) {
                File file = new File(listImageCancel.get(i));
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        String jsonIds = mGson.toJson(routing_plan_day_id);
        RequestBody ids = RequestBody.create(MediaType.parse("application/json"), jsonIds);

        Call<ResponseBody> call = soService.notContactCustomer(mOdoo.getSessionCokie(), ids, 0, time, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void getListRatingBadges(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("type", "CUSTOMER");
            mOdoo.callRoute(ROUTE_LIST_RATING_BADGES, params, new SharingOdooResponse<OdooResultDto<RatingBadges>>() {
                @Override
                public void onSuccess(OdooResultDto<RatingBadges> resultDto) {
                    if (resultDto != null && resultDto.getRecords().size() > 0)
                        result.onSuccess(resultDto.getRecords());
                    else
                        result.onSuccess(null);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onSuccess(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void ratingCustomer(RatingCustomerDTO ratingCustomerDTO, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("rating", new JSONObject(mGson.toJson(ratingCustomerDTO)));
            mOdoo.callRoute(ROUTE_RATING_CUSTOMER, params, new SharingOdooResponse<Boolean>() {

                @Override
                public void onSuccess(Boolean aBoolean) {

                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onSuccess(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getCustomerInfo(Integer routing_plan_day_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id", routing_plan_day_id);
            mOdoo.callRoute(ROUTE_GET_CUSTOMER_INFO, params, new SharingOdooResponse<Partner>() {
                @Override
                public void onSuccess(Partner resultDto) {
                    if (resultDto != null)
                        result.onSuccess(resultDto);
                    else
                        result.onSuccess(null);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onSuccess(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getRouting(String currentDate, long driverId, String search, ArrayList<Integer> status, IResponse result) {
        JSONObject params = new JSONObject();
        try {
            params.put("date", currentDate);
            params.put("bill_routing_code", search);
            params.put("driver_id", driverId);
            params.put("status", new JSONArray(mGson.toJson(status)));
            mOdoo.callRoute(ROUTE_ROUTING, params, new SharingOdooResponse<OdooResultDto<ShareVanRoutingPlan>>() {
                @Override
                public void onSuccess(OdooResultDto<ShareVanRoutingPlan> shareVanRoutingPlans) {
                    if (shareVanRoutingPlans != null) {
                        result.onSuccess(shareVanRoutingPlans.getRecords());
                    } else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getRoutingHistory(String fromDate, String toDate, long driverId, List<Integer> status, int page, String bill_routing_code, IResponse result) {
        JSONObject params = new JSONObject();
        try {
            params.put("from_date", fromDate);
            params.put("to_date", toDate);
            params.put("offset", page * 10);
            params.put("limit", 10);
            params.put("driver_id", driverId);
            params.put("status", new JSONArray(mGson.toJson(status)));
            if (bill_routing_code != null)
                params.put("bill_routing_code", bill_routing_code);
            mOdoo.callRoute(ROUTE_ROUTING, params, new SharingOdooResponse<OdooResultDto<ShareVanRoutingPlan>>() {
                @Override
                public void onSuccess(OdooResultDto<ShareVanRoutingPlan> shareVanRoutingPlans) {
                    if (shareVanRoutingPlans != null) {
                        result.onSuccess(shareVanRoutingPlans);
                    } else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void getRoutingDetail(String routingCode, IResponse result) {
        JSONObject params = new JSONObject();
        try {
            params.put("routing_plan_day_code", routingCode);
            mOdoo.callRoute(ROUTE_ROUTING_DETAIL, params, new SharingOdooResponse<OdooResultDto<ShareVanRoutingPlan>>() {
                @Override
                public void onSuccess(OdooResultDto<ShareVanRoutingPlan> shareVanRoutingPlans) {
                    result.onSuccess(shareVanRoutingPlans);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportBillPackageChange(String description, ShareVanRoutingPlan routingPlan, List<LocalMedia> lstFileSelected, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        MultipartBody.Part[] parts = null;
        routingPlan.setDescription(description);
        String json = mGson.toJson(routingPlan);

        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);

        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        Call<ApiResponseModel> call = soService.reportBillPackageChange(mOdoo.getSessionCokie()
                , bodyJson, parts);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                if (response.body() != null) {
                    result.onSuccess(response.body());
                } else {
                    result.onFail(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                result.onFail(t);
            }
        });

//        JSONObject params = new JSONObject();
//        try {
//            params.put("routing_plan_day", new JSONObject(mGson.toJson(routingPlan)));
//            mOdoo.callRoute("/share_van_order/update_routing_plan_day", params, new SharingOdooResponse<OdooResultDto<ShareVanRoutingPlan>>() {
//                @Override
//                public void onSuccess(OdooResultDto<ShareVanRoutingPlan> shareVanRoutingPlans) {
//                    result.onSuccess(shareVanRoutingPlans);
//                }
//
//                @Override
//                public void onFail(Throwable error) {
//                    result.onFail(error);
//                }
//
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void confirmRouting(String description, ShareVanRoutingPlan routingDay, List<LocalMedia> lstFileSelected, IResponse result) {

        SOService soService = mOdoo.getClient().createService(SOService.class);

        Integer routingId = routingDay.getId();
        MultipartBody.Part[] parts = null;
        String json = "{\"id\":" + routingId
                + ",\"routing_plan_day_code\":\"" + routingDay.getRouting_plan_day_code()
                + "\", \"status\": \"1\""
                + ", \"so_type\": " + routingDay.getSo_type()
                + " , \"description\":\"" + description + "\"}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("ufile", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        Call<ResponseBody> call = soService.confirmRouting(mOdoo.getSessionCokie()
                , bodyJson, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    result.onSuccess(response.body());
                } else {
                    result.onFail(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onFail(t);
            }
        });
    }

    public static void confirmArrived(List<Integer> listRoutingPlanId, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id", new JSONArray(mGson.toJson(listRoutingPlanId)));

            mOdoo.callRoute("/routing/accept_warehouse_place", params, new SharingOdooResponse<String>() {
                @Override
                public void onSuccess(String obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

}
