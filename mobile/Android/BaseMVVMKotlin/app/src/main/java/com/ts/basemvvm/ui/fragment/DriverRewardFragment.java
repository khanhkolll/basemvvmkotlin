package com.ts.basemvvm.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.basemvvm.R;
import com.ts.basemvvm.databinding.DriverRewardFragmentBinding;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DriverRewardFragment extends BaseFragment {
    DriverRewardFragmentBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (DriverRewardFragmentBinding) binding;

        return v;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.driver_reward_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
