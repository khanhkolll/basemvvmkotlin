package com.ts.basemvvm.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationModel extends BaseModel {
    private Long id;
    private Long notification_id;
    private OdooDateTime create_date;
    private Boolean is_read;
    private String click_action;
    private String type;
    private String title;
    private String content;
    private String description;
    private String object_status;
    private String item_id;
    private String image_256;


}
