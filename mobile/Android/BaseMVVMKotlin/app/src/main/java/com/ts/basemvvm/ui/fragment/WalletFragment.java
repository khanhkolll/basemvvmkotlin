package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.PagerAdapter;
import com.ts.basemvvm.databinding.WalletFragmentBinding;
import com.ts.basemvvm.viewmodel.WalletVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class WalletFragment extends BaseFragment {
    WalletVM walletVM;
    WalletFragmentBinding mBinding;
    private MenuItem preItem;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        walletVM = (WalletVM) viewModel;
        mBinding = (WalletFragmentBinding) binding;
        initView();
        return view;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void initView() {
        mBinding.toolbar.setTitle(R.string.wallet);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
//        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
//        if (appCompatActivity.getSupportActionBar() != null) {
//            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }

        mBinding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.icon_back));
        mBinding.toolbar.setNavigationOnClickListener(v -> getBaseActivity().onBackPressed());
        setUpViewPager();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnShowMoney) {
            if (walletVM.getShowMoney().get() && walletVM.getShowMoney() != null) {
                walletVM.getShowMoney().set(false);
            } else {
                walletVM.getShowMoney().set(true);
            }
        }
    }

    private void setUpViewPager() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());

        WalletChildFragment history = new WalletChildFragment();
        StatisticalFragment statistical = new StatisticalFragment();

        //add Fragment to list fragment
        myPagerAdapter.addFragment(history);
        myPagerAdapter.addFragment(statistical);

        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nvHistory:
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.nvStatistical:
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.wallet_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WalletVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
