package com.ts.basemvvm.service.location_service;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LocLatLng implements Serializable {
    public double latitude;
    public double longitude;
    public String time;

    public LocLatLng() {
    }

    public LocLatLng(double latitude, double longitude, String time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = time;
    }

}
