package com.ts.basemvvm.enums;

public class  VehicleStateStatus {
    public static final String Waiting = "Waiting";
    public static final String Available = "Available";
    public static final String Ordered = "Ordered";
    public static final String Downgraded = "Downgraded";
    public static final String Shipping = "Shipping";
    public static final String Maintenance = "Maintenance";

    public static final String ConfirmTake = "ConfirmTake";
    public static final String ConfirmReturn = "ConfirmReturn";
}
