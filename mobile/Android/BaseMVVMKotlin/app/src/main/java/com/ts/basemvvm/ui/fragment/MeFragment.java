package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.MeFragmentBinding;
import com.ts.basemvvm.enums.VehicleStateStatus;
import com.ts.basemvvm.service.location_service.DatabaseHelper;
import com.ts.basemvvm.ui.activity.LoginActivityV2;
import com.ts.basemvvm.ui.activity.RoutingHistoryActivity;
import com.ts.basemvvm.ui.activity.WorkingScheduleActivity;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.utils.ZoomImage;
import com.ts.basemvvm.viewmodel.MeVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import java.util.Date;
import java.util.Objects;

public class MeFragment extends BaseFragment {
    MeFragmentBinding mBinding;
    MeVM meVM;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (MeFragmentBinding) binding;
        meVM = (MeVM) viewModel;
        if (StaticData.getDriver().getExpires_date() != null && checkExpiresDate()) {
            mBinding.txtExpiresDate.setTextColor(getResources().getColor(R.color.color_price));
        }
        meVM.getDriverObservable().set(StaticData.getDriver());
        return v;
    }

    public boolean checkExpiresDate() {
        if (StaticData.getDriver().getExpires_date().getTime() < (new Date()).getTime()) {
            return true;
        }
        return false;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.btnProfile:
            case R.id.layoutHeader:
                openProfileFragment();
                break;
            case R.id.imgDriver:
                ZoomImage.zoom(this, StaticData.getDriver().getImage_1920());
                break;
            case R.id.btnWallet:
                gotoWallet();
                break;
            case R.id.btnWorkingSchedule:
                openWorkingSchedule();
                break;

            case R.id.btnVehicleHistory:
                openVehicleHistory();
                break;

            case R.id.btnHistory:
                openRoutingHistory();
                break;

            case R.id.btnRelief:
                showReliefDialog();
                break;
            case R.id.btnLogout:
                showDialogLogout();
                break;
            case R.id.btnChangePassword:
                changePassword();
                break;
        }
    }

    public void changePassword() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        changePasswordDialog.setTargetFragment(this, Constants.CHANGE_PASSWORD);
        changePasswordDialog.show(fm, "ABC");
    }

    public void getDriverReward() {
        meVM.getDriverReward();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.CHANGE_PASSWORD && resultCode == Constants.RESULT_OK) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.CHANGE_PASSWORD_SUCCESS), getResources().getDrawable(R.drawable.ic_check));
            new Handler().postDelayed(() -> {
                logout();
            }, 1000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDriverReward();
        if (StaticData.getDriver() != null) {
            if (StaticData.getDriver().getVehicle() == null) {
                mBinding.txtLicensePlate.setVisibility(View.GONE);
                mBinding.txtStatus.setVisibility(View.GONE);
            } else {
                mBinding.txtStatus.setVisibility(View.VISIBLE);
                mBinding.txtLicensePlate.setVisibility(View.VISIBLE);
                mBinding.txtLicensePlate.setText(StaticData.getDriver().getVehicle().getLicense_plate());
                switch (StaticData.getDriver().getVehicle().getStatus()) {
                    case VehicleStateStatus.Shipping:
                        mBinding.txtStatus.setText(getString(R.string.ship));
                        break;
                    case VehicleStateStatus.Ordered:
                        mBinding.txtStatus.setText(getString(R.string.parking));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void openProfileFragment() {
        Intent intent = new Intent(requireActivity(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, ProfileFragment.class);
        requireActivity().startActivity(intent);
    }

    public void openRoutingHistory() {
        Intent intent = new Intent(requireActivity(), RoutingHistoryActivity.class);
        requireActivity().startActivity(intent);
    }

    public void openVehicleHistory() {
        Intent intent = new Intent(requireActivity(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, VehicleHistoryFragment.class);
        requireActivity().startActivity(intent);
    }

    public void openWorkingSchedule() {
        Intent intent = new Intent(requireActivity(), WorkingScheduleActivity.class);
        requireActivity().startActivity(intent);
    }

    public void showReliefDialog() {
        ReliefDialog reliefDialog = new ReliefDialog();
        reliefDialog.show(getFragmentManager(), "ABC");
    }

    public void gotoWallet() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, WalletFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void showDialogLogout() {
        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.message_logout), "").setOnClickListener(v -> {
            logout();
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    private void logout() {
        // Delete token firebase
        meVM.logout();
        // Delete user & pass
        AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
        // Delete cache
        StaticData.setOdooSessionDto(null);
        StaticData.setDriver(null);
        StaticData.sessionCookie = "";
        // Delete local db
        Objects.requireNonNull(getContext()).deleteDatabase(DatabaseHelper.DATABASE_NAME);
        // Intent loginActivity
        Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void updateData() {
        meVM.getUserInfo(this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals("Get Info Driver Success")) {
            //Do something
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.me_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
