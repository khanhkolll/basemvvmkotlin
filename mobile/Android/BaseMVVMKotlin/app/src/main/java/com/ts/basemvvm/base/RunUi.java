package com.ts.basemvvm.base;

public interface RunUi {
    void run(Object... params);
}
