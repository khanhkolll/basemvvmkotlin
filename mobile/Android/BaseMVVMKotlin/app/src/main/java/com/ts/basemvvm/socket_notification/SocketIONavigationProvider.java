package com.ts.basemvvm.socket_notification;

public interface SocketIONavigationProvider {
    SocketIONavigation socketNavigation();
}
