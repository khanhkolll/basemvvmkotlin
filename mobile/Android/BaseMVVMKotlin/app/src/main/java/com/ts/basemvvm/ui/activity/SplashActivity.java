package com.ts.basemvvm.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.android.volley.VolleyError;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.api.BaseApi;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.ActivitySplashBinding;
import com.ts.basemvvm.model.UserInfo;
import com.ts.basemvvm.utils.ApiResponse;
import com.ts.basemvvm.utils.LanguageUtils;
import com.ts.basemvvm.utils.NetworkUtils;
import com.ts.basemvvm.utils.StringUtils;
import com.ts.basemvvm.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> {
    Bundle extras;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = AppController.getInstance().getSharePre();
        // TODO: 23/09/2020 configServer: OdooClient client = new OdooClient(this, AppController.BASE_URL);
        OdooClient client = new OdooClient(this, preferences.getString("U_LOGIN", AppController.SERVER_URL) + ":8070");
        onConnect(client, null);

        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        binding.animationView.setAnimation("animation/delivery_tracking_animation.json");
        binding.animationView.playAnimation();
    }

    private void startLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivityV2.class));
        // close splash activity
        finish();
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case SUCCESS:
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                startLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public void onConnect(OdooClient odooV2, VolleyError volleyError) {
        LanguageUtils.loadLocale(SplashActivity.this);
        BaseApi.setOdoo(odooV2);
        LoginVM loginVM = new LoginVM(getApplication());
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass) && NetworkUtils.isNetworkConnected(SplashActivity.this)) {
                loginVM.getModel().set(UserInfo.builder().userName(userName).passWord(pass).build());
                loginVM.requestLogin();
            } else {
                startLogin();
            }
        } else {
            startLogin();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);
    }
}
