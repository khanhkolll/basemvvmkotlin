package com.ts.basemvvm.adapter.provider;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.chad.library.BR;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.RoutingNodeAdapter;
import com.ts.basemvvm.model.section.RootNode;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public class HeaderNodeProvider extends BaseNodeProvider {
    private final int idLayoutItem;
    private final BaseViewModel baseViewModel;

    public HeaderNodeProvider(int idLayoutItem, BaseViewModel baseViewModel) {
        this.idLayoutItem = idLayoutItem;
        this.baseViewModel = baseViewModel;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return idLayoutItem;
    }

    @Override
    public void convert(@NonNull BaseViewHolder helper, @NonNull BaseNode data, @NonNull List<?> payloads) {
        for (Object payload : payloads) {
            if (payload instanceof Integer && (int) payload == RoutingNodeAdapter.EXPAND_COLLAPSE_PAYLOAD) {
                spinArrow(helper, data);
            }
        }
    }

    private void spinArrow(BaseViewHolder helper, BaseNode data) {
        RootNode entity = (RootNode) data;
        ImageView imageView = helper.getView(R.id.expand);

        if (entity.isExpanded()) {
            ViewCompat.animate(imageView).setDuration(200)
                    .setInterpolator(new DecelerateInterpolator())
                    .rotation(270f)
                    .start();
        } else {
            ViewCompat.animate(imageView).setDuration(200)
                    .setInterpolator(new DecelerateInterpolator())
                    .rotation(90f)
                    .start();
        }
    }

    @Override
    public void convert(@NonNull BaseViewHolder holder, @Nullable BaseNode data) {
        RootNode entity = (RootNode) data;
        assert entity != null;
        spinArrow(holder, data);
        ViewDataBinding binding = DataBindingUtil.bind(holder.itemView);
        if (binding != null) {
            binding.setVariable(BR.viewHolder, entity);
            binding.setVariable(BR.viewModel, baseViewModel);
            binding.executePendingBindings();
        }
    }

    @Override
    public void onClick(@NonNull BaseViewHolder helper, @NonNull View view, BaseNode data, int position) {
    }
}
