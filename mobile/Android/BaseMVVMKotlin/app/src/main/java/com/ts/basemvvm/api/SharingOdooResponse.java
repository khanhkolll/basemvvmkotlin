package com.ts.basemvvm.api;

import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ts.basemvvm.base.IResponse;
import com.workable.errorhandler.ErrorHandler;

public abstract class SharingOdooResponse<Result> implements IOdooResponse<Result>, IResponse<Result> {
    @Override
    public void onResponse(Result result, Throwable volleyError) {
        if(volleyError != null){
            volleyError.printStackTrace();
            ErrorHandler.create().handle(volleyError);
            onFail(volleyError);
            return;
        }
        onSuccess(result);
    }
}
