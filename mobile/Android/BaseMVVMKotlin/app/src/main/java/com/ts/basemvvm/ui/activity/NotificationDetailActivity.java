package com.ts.basemvvm.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;

import com.ts.basemvvm.R;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.ActivityNotificationDetailBinding;
import com.ts.basemvvm.viewmodel.NotificationDetailVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class NotificationDetailActivity extends BaseActivity<ActivityNotificationDetailBinding> {
    NotificationDetailVM notificationDetailVM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationDetailVM = (NotificationDetailVM) viewModel;

        String notification_id = getIntent().getExtras().getString(Constants.ITEM_ID);

        notificationDetailVM.getNotificationById(notification_id);
        initToolbar();
    }

    private void initToolbar() {
        binding.toolbar.setTitle(R.string.notification_detail);
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.activity_notification_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}