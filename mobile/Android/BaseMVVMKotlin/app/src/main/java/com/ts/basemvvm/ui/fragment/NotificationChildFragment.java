package com.ts.basemvvm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.BaseAdapterV3;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.NotificationChildFragmentBinding;
import com.ts.basemvvm.enums.ClickAction;
import com.ts.basemvvm.enums.ObjectStatus;
import com.ts.basemvvm.model.NotificationModel;
import com.ts.basemvvm.utils.NetworkUtils;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.NotificationVM;
import com.ts.basemvvm.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class NotificationChildFragment extends BaseFragment {
    int layoutItem;
    String type;
    NotificationChildFragmentBinding mBinding;
    NotificationVM notificationVM;

    BaseAdapterV3 notificationAdapter;

    public NotificationChildFragment(@LayoutRes int layoutItem, String type) {
        this.layoutItem = layoutItem;
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (NotificationChildFragmentBinding) binding;
        notificationVM = (NotificationVM) viewModel;

        initView();
        initLoadMore();
        onRefresh();
        return v;
    }

    public void onRefresh() {
        notificationVM.getNotification(0, type, this::runUi);
    }

    private void initLoadMore() {
        notificationAdapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            notificationVM.getMoreNotifications(type, this::runUi);
        });
        notificationAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        notificationAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getNotificationSuccess":
                notificationAdapter.getLoadMoreModule().loadMoreComplete();
                notificationAdapter.notifyDataSetChanged();
                emptyData();
                break;
            case "getNotificationFail":
                notificationAdapter.notifyDataSetChanged();
                notificationAdapter.getLoadMoreModule().loadMoreComplete();
                emptyData();
                break;
            case "noMore":
                notificationAdapter.getLoadMoreModule().loadMoreEnd();
                break;
        }
    }

    private void initView() {
        notificationAdapter = new BaseAdapterV3(this.layoutItem, notificationVM.getListNotification(), this);
        mBinding.rcNotification.setAdapter(notificationAdapter);
        notificationAdapter.addChildClickViewIds(R.id.itemNotification);

        mBinding.swRefresh.setOnRefreshListener(() -> {
            notificationVM.onRefresh(type, this::runUi);
            notificationAdapter.notifyDataSetChanged();
        });
    }

    public void pushNotification(NotificationModel notificationModel) {
        notificationVM.getListNotification().add(0, notificationModel);
        notificationAdapter.notifyItemInserted(0);
        mBinding.rcNotification.scrollToPosition(0);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        NotificationModel notificationModel = (NotificationModel) o;
        if (!notificationModel.getIs_read()) {
            notificationVM.getListNotification().get(notificationModel.index - 1).setIs_read(true);
            notificationVM.seenNotification(notificationVM.getListNotification().get(notificationModel.index - 1).getId());
            notificationAdapter.notifyItemChanged(notificationModel.index - 1);
        }
        Intent intent = new Intent(notificationModel.getClick_action());
        if (ClickAction.ROUTING_HISTORY.equals(notificationModel.getClick_action())) {
            intent.putExtra("NOTIFICATION_DATE", notificationModel.getCreate_date().getTime());
        }
        intent.putExtra(Constants.ITEM_ID, notificationModel.getItem_id() + "");

        if (StaticData.getDriver().getVehicle() == null
                && notificationModel.getClick_action().equals(ClickAction.VEHICLE_INFO)) {
            ToastUtils.showToast(getActivity(), getString(R.string.mse_not_use_any_vehicle), getResources().getDrawable(R.drawable.ic_vans));
            return;
        }

        if (ObjectStatus.FIVE.equals(notificationModel.getObject_status()) && notificationModel.getClick_action().equals(ClickAction.VEHICLE_INFO)) {
            ToastUtils.showToast(getActivity(), notificationModel.getTitle(), getResources().getDrawable(R.drawable.ic_vans));
            return;
        }

        if (StaticData.getDriver().getAssignation_log() != null
                && !StaticData.getDriver().getAssignation_log().getId().equals(notificationModel.getId())
                && notificationModel.getClick_action().equals(ClickAction.VEHICLE_INFO)) {
            ToastUtils.showToast(getActivity(), getString(R.string.msg_not_map_calendar_vehicle), getResources().getDrawable(R.drawable.ic_vans));
            return;
        }

        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void emptyData() {
//        if (arrayList != null && arrayList.size() > 0) {
//            mBinding.emptyData.setVisibility(View.GONE);
//            mBinding.rcNotification.setVisibility(View.VISIBLE);
//        } else {
//            mBinding.emptyData.setVisibility(View.VISIBLE);
//            mBinding.rcNotification.setVisibility(View.GONE);
//        }

        if ((notificationVM.getRecords().get() == 0 || notificationVM.getRecords().get() > 0) && !NetworkUtils.isNetworkConnected(getActivity())) {
            mBinding.emptyData.setVisibility(View.VISIBLE);
            mBinding.llEmptyData.setVisibility(View.GONE);
        }

        if (notificationVM.getRecords().get() > 0 && NetworkUtils.isNetworkConnected(getActivity())) {
            mBinding.emptyData.setVisibility(View.GONE);
            mBinding.llEmptyData.setVisibility(View.GONE);
        }

        if (notificationVM.getRecords().get() == 0 && NetworkUtils.isNetworkConnected(getActivity())) {
            mBinding.emptyData.setVisibility(View.GONE);
            mBinding.llEmptyData.setVisibility(View.VISIBLE);
        }
    }
}
