package com.ts.basemvvm.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.StaticData;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletDTO extends BaseModel {
    private String url_path;
    private Long id;
    private String code;
    private Long driver_level_id;
    private Double percent_commission;
    private Object coupon_id;
    private Double amount;
    private Double total_amount;
    private Long create_uid;
    private OdooDateTime create_date;
    private Long write_uid;
    private OdooDateTime write_date;
    private Boolean pay_check;
    private Long discount_amount;
    private String status;
    private Long driver_id;
    private Long order_id;
    private String order_type;
    private String name;
    private Double commission;
//     receive_type = 0 cộng tiền
//     receive_type = 1 trừ tiền
//     receive_type = 2 tất cả
    private String receive_type;
    public String getTotalAmountStr(){
        return AppController.getInstance().formatNumber(this.total_amount)+" "+ StaticData.getOdooSessionDto().getCurrency();
    }
    public String getCommissionStr(){
        return AppController.getInstance().formatNumber(this.commission)+" "+ StaticData.getOdooSessionDto().getCurrency();
    }
    public String getAmountStr(){
        return AppController.getInstance().formatNumber(this.amount)+" "+ StaticData.getOdooSessionDto().getCurrency();
    }
}
