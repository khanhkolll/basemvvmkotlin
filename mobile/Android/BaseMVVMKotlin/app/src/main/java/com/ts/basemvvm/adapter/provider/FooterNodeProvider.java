package com.ts.basemvvm.adapter.provider;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.ts.basemvvm.R;
import com.ts.basemvvm.model.section.RootFooterNode;

public class FooterNodeProvider extends BaseNodeProvider {
    private int idLayoutItem;

    public FooterNodeProvider(int idLayoutItem) {
        this.idLayoutItem = idLayoutItem;
        addChildClickViewIds(R.id.footerTv);
    }

    @Override
    public int getItemViewType() {
        return 2;
    }

    @Override
    public int getLayoutId() {
        return idLayoutItem;
    }

    @Override
    public void convert(@NonNull BaseViewHolder helper, @Nullable BaseNode data) {
        RootFooterNode entity = (RootFooterNode) data;
        assert entity != null;
        helper.setText(R.id.footerTv, entity.getTitle());
    }

    @Override
    public void onChildClick(@NonNull BaseViewHolder helper, @NonNull View view, BaseNode data, int position) {
    }
}
