package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.ts.basemvvm.api.RoutingApi;
import com.ts.basemvvm.api.SharingOdooResponse;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.model.Partner;
import com.ts.basemvvm.model.RatingBadges;
import com.ts.basemvvm.model.RatingCustomerDTO;
import com.ts.basemvvm.utils.StringUtils;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatingCustomerVM extends BaseViewModel {
    ObservableBoolean isLoading = new ObservableBoolean();
    ObservableBoolean isBtnRating = new ObservableBoolean();
    ObservableField<String> description = new ObservableField<>();
    ObservableField<Partner> customer = new ObservableField<>();
    HashMap<String, List<RatingBadges>> hashMapRatingBadges = new HashMap<>();
    ObservableList<RatingBadges> ratingBadges = new ObservableArrayList<>();

    public RatingCustomerVM(@NonNull Application application) {
        super(application);
        isLoading.set(true);
        isBtnRating.set(true);
    }

    public void getCustomerInfo(Integer routing_plan_day_id, RunUi runUi) {
        RoutingApi.getCustomerInfo(routing_plan_day_id, new SharingOdooResponse() {

            @Override
            public void onSuccess(Object o) {
                customer.set((Partner) o);
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }

    public void getDriverRatingBadges(RunUi runUi) {
        RoutingApi.getListRatingBadges(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
//                    ratingBadges.addAll((Collection<? extends RatingBadges>) o);
                    List<RatingBadges> ratingBadges = (List<RatingBadges>) o;
                    setDataHashMapRatingBadges(ratingBadges);
                    runUi.run(Constants.SUCCESS_API);
                } else {
                    runUi.run(Constants.FAIL_API);
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }

    public void setDataHashMapRatingBadges(List<RatingBadges> data) {
        for (RatingBadges item : data) {
            List<RatingBadges> listTemp = new ArrayList<>();
            if (hashMapRatingBadges.get(item.getRating_level()) != null) {
                listTemp=hashMapRatingBadges.get(item.getRating_level());
            }
            listTemp.add(item);
            hashMapRatingBadges.put(item.getRating_level(), listTemp);
        }
        if (hashMapRatingBadges.get("1") != null)
            ratingBadges.addAll(hashMapRatingBadges.get("1"));
    }

    public void handleData(Integer routing_id, Integer numberStar, RunUi runUi) {
        //lay id ratingBadges da chon
        try {
            List<Integer> integers = new ArrayList<>();
            for (RatingBadges item : ratingBadges) {
                if (item.isCheck()) {
                    integers.add(item.getId());
                }
            }
            RatingCustomerDTO ratingCustomerDTO = new RatingCustomerDTO();
            ratingCustomerDTO.setList_badges(integers);
            if (StringUtils.isNullOrEmpty(description.get())) {
                ratingCustomerDTO.setNote("");
            } else {
                ratingCustomerDTO.setNote(description.get());
            }
            ratingCustomerDTO.setDriver_id(StaticData.getDriver().getId());
            ratingCustomerDTO.setEmployee_id(customer.get().getId());
            ratingCustomerDTO.setRating(numberStar);
            ratingCustomerDTO.setRating_place_id(routing_id);
            ratingCustomerConfirm(ratingCustomerDTO, runUi);
        } catch (Exception e) {
            runUi.run(Constants.RATING_FAIL);
        }

    }

    public void ratingCustomerConfirm(RatingCustomerDTO ratingCustomerDTO, RunUi runUi) {
        RoutingApi.ratingCustomer(ratingCustomerDTO, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    runUi.run(Constants.RATING_SUCCESS);
                } else {
                    runUi.run(Constants.RATING_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }
}
