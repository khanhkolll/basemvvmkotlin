package com.ts.basemvvm.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.basemvvm.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.Calendar;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticalWalletDTO extends BaseModel {
    Long id;
    OdooDateTime month;
    Float total_save;
    Float total_pay;
    Float total_amount;
    public String getTotal_saveStr(){
        return AppController.getInstance().formatCurrency(total_save);
    }
    public String getTotal_payStr(){
        return AppController.getInstance().formatCurrency(total_pay);
    }
    public String getTotal_amountStr(){
        return AppController.getInstance().formatCurrency(total_save+total_pay);
    }

    public String getCreateDateStr() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.month);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        if (month < 10) {
            return "0" + month + "/" + year;
        } else
            return month + "/" + year;
    }
    public Integer getMonthInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.month);
        return cal.get(Calendar.MONTH) + 1;
    }
    public Integer getYearInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.month);
        return cal.get(Calendar.YEAR);
    }

}
