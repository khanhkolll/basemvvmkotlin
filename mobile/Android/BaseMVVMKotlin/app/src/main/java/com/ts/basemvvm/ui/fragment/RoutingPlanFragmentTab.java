package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.PagerAdapter;
import com.ts.basemvvm.api.SOSApi;
import com.ts.basemvvm.base.INotifyDataRouting;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.RoutingFragmentTabBinding;
import com.ts.basemvvm.enums.SosStatus;
import com.ts.basemvvm.model.mBool;
import com.ts.basemvvm.utils.ToastUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

public class RoutingPlanFragmentTab extends DialogFragment {
    private MenuItem preItem;

    private RoutingFragmentTabBinding mBinding;
    private final OnDismiss onDismiss;
    private final mBool isChanged;
    private RoutingPlanFragment pending;
    private RoutingPlanFragment success;

    public RoutingPlanFragmentTab(RoutingPlanFragmentTab.OnDismiss onDismiss) {
        this.onDismiss = onDismiss;
        this.isChanged = new mBool(false);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.routing_fragment_tab, container, false);
        initView();
        if (StaticData.getDriver().getVehicle() != null) {
            if(SosStatus.CONTINUABLE.equals(StaticData.getDriver().getVehicle().getSos_status())){
                initResoldSos();
            }
        }
        return mBinding.getRoot();
    }

    @SuppressLint("NonConstantResourceId")
    private void initView() {
        mBinding.btnBack.setOnClickListener(v -> this.dismiss());

        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());

        pending = new RoutingPlanFragment(0, isChanged);
        success = new RoutingPlanFragment(1, isChanged);
        pending.setINotifyDataRouting(new INotifyDataRouting() {
            @Override
            public void notifyData(boolean isChange) {
                if (isChange) {
                    success.onRefreshData();
                }
            }
        });
        success.setINotifyDataRouting(new INotifyDataRouting() {
            @Override
            public void notifyData(boolean isChange) {
                if (isChange) {
                    pending.onRefreshData();
                }
            }
        });

        myPagerAdapter.addFragment(pending);
        myPagerAdapter.addFragment(success);

        mBinding.content.setAdapter(myPagerAdapter);

        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nvPending:
                    mBinding.content.setCurrentItem(0);
                    break;
                case R.id.nvSuccess:
                    mBinding.content.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.content.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    private void initResoldSos() {
        mBinding.btnResoldSOS.setVisibility(View.VISIBLE);
        mBinding.btnResoldSOS.setAnimation("animation/sos_animation.json");
        mBinding.btnResoldSOS.playAnimation();
        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.confirm_the_problem_is_fixed)
                , getString(R.string.msg_confirm_resold_sos)).setAnimationFile("animation/success_animation.json");
        dialogConfirm.setOnClickListener(v -> {
            updateSosStatus();
            dialogConfirm.dismiss();
        });

        mBinding.btnResoldSOS.setOnClickListener(v -> {
            dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
        });

    }

    //lớp này ko có tào VM nên viết tạm xử lý ở view luôn.
    private void updateSosStatus(){
        SOSApi.updateSosStatus(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if("200".equals(o)){
                    StaticData.getDriver().getVehicle().setSos_status(SosStatus.NOT_SOS);
                    mBinding.btnResoldSOS.setVisibility(View.GONE);
                    ToastUtils.showToast(getActivity(), getString(R.string.msg_confirm_resold_sos), getResources().getDrawable(R.drawable.ic_check));
                }else {
                    ToastUtils.showToast(getActivity(), getString(R.string.msg_confirm_resold_fail), getResources().getDrawable(R.drawable.ic_danger));
                }
            }

            @Override
            public void onFail(Throwable error) {
                ToastUtils.showToast(getActivity(), getString(R.string.msg_confirm_resold_fail), getResources().getDrawable(R.drawable.ic_danger));
            }
        });
    }

    public void refresh(){
//        ArrayList<Integer> statusPending = new ArrayList<>();
//        statusPending.add(0);
//        statusPending.add(1);
//        statusPending.add(4);
//        ArrayList<Integer> statusSuccess = new ArrayList<>();
//        statusSuccess.add(2);
//        statusSuccess.add(3);

//        pending.onRefreshData(statusPending);
//        success.onRefreshData(statusSuccess);
        pending.onRefreshData();
        success.onRefreshData();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        onDismiss.onDismiss(isChanged);
    }

    public interface OnDismiss {
        void onDismiss(mBool isChange);
    }
}
