/*
 * Copyright 2015 - 2019 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ts.basemvvm.service.location_service;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;

public class TrackingController implements PositionProvider.PositionListener, NetworkManager.NetworkHandler {

    private static final String TAG = TrackingController.class.getSimpleName();
    private static final int RETRY_DELAY = 30 * 1000;

    private boolean isOnline;
    private boolean isWaiting;

    private Context context;
    private Handler handler;

    private String url;

    private PositionProvider positionProvider;
    private DatabaseHelper databaseHelper;
    private NetworkManager networkManager;

    public TrackingController(Context context) {
        this.context = context;
        handler = new Handler();
        positionProvider = new AndroidPositionProvider(context, this);
        databaseHelper = new DatabaseHelper(context);
        networkManager = new NetworkManager(context, this);
        isOnline = networkManager.isOnline();

        url = AppController.LOCATION_SERVICE_URL;
    }

    public void setConfig(String deviceId, long interval, double distance, double angle){
        positionProvider.setConfig(deviceId, interval, distance, angle);
    }

    public void start() {
        if (isOnline) {
            read();
        }
        try {
            positionProvider.startUpdates();
        } catch (SecurityException e) {
            Log.w(TAG, e);
        }
        networkManager.start();
    }

    public void stop() {
        networkManager.stop();
        try {
            positionProvider.stopUpdates();
        } catch (SecurityException e) {
            Log.w(TAG, e);
        }
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onPositionUpdate(Position position) {
        Log.e(TAG, "update location");
        if (position != null ) {
            write(position);
        }
    }

    @Override
    public void onPositionError(Throwable error) {
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        int message = isOnline ? R.string.status_network_online : R.string.status_network_offline;
        Log.e(TAG, "update network");

        if (!this.isOnline && isOnline) {
            read();
        }
        this.isOnline = isOnline;
    }

    //
    // State transition examples:
    //
    // write -> read -> send -> delete -> read
    //
    // read -> send -> retry -> read -> send
    //

    private void log(String action, Position position) {
        if (position != null) {
            action += " (" +
                    "id:" + position.getId() +
                    " time:" + position.getTime().getTime() / 1000 +
                    " lat:" + position.getLatitude() +
                    " lon:" + position.getLongitude() + ")";
        }
        Log.d(TAG, action);
    }

    private void write(Position position) {
        databaseHelper.insertPositionAsync(position, (success, result) -> {
            log("write", position);
            if (success) {
                if (isOnline && isWaiting) {
                    read();
                    isWaiting = false;
                }
            }
        });
    }

    private void read() {
        log("read", null);
        databaseHelper.selectPositionAsync((success, result) -> {
            if (success) {
                if (result != null) {
                    send(result);
                }else {
                    isWaiting = true;
                }
            } else {
                retry();
            }
        });
    }

    private void delete(Position position) {
        log("delete", position);
        databaseHelper.deletePositionAsync(position.getId(), (success, result) -> {
            if (success) {
                read();
            } else {
                retry();
            }
        });
    }

    private void send(final Position positions) {
        log("send ", positions);
        String request = ProtocolFormatter.formatRequest(url, positions);
        RequestManager.sendRequestAsync(request, success -> {
            if (success) {
                log("send success", positions);
                delete(positions);
            } else {
                Log.e("send fail", positions + "");
                retry();
            }
        });
    }

    private void retry() {
        log("retry", null);
        handler.postDelayed(() -> {
            if (isOnline) {
                read();
            }
        }, RETRY_DELAY);
    }

}
