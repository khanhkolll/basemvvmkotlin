package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.ts.basemvvm.R;
import com.ts.basemvvm.databinding.DialogEditQuantityBinding;
import com.ts.basemvvm.databinding.DialogWalletTransactionHistoryBinding;
import com.ts.basemvvm.model.WalletDTO;
import com.ts.basemvvm.utils.BindingAdapterUtil;


@SuppressLint("ValidFragment")
public class DialogWalletTransactionHistory extends DialogFragment {
    DialogWalletTransactionHistoryBinding mBinding;
    WalletDTO walletDTO;

    public DialogWalletTransactionHistory(WalletDTO walletDTO) {
        this.walletDTO = walletDTO;
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_wallet_transaction_history, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        mBinding.btnClose.setOnClickListener(v -> {
            this.dismiss();
        });
        initView();
        return mBinding.getRoot();

    }

    private void initView() {
        mBinding.txtCommission.setText(walletDTO.getCommissionStr());
        BindingAdapterUtil.calculatorTime(mBinding.txtCreateDate, walletDTO.getCreate_date());
        mBinding.txtName.setText(walletDTO.getName());
        mBinding.txtTotal.setText(walletDTO.getAmountStr());
        if (walletDTO.getReceive_type().equals("0"))
            mBinding.txtTotalAmount.setText("+" + walletDTO.getTotalAmountStr());
        else
            mBinding.txtTotalAmount.setText("+" + walletDTO.getTotalAmountStr());
        if (walletDTO.getPay_check()) {
            mBinding.txtStatus.setText(getString(R.string.success));
            mBinding.txtStatus.setTextColor(getResources().getColor(R.color.primaryColor));
        } else {
            mBinding.txtStatus.setText(getString(R.string.FAIL));
            mBinding.txtStatus.setTextColor(getResources().getColor(R.color.accent));
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
