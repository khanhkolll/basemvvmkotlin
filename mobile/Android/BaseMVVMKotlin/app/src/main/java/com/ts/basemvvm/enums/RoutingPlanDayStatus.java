package com.ts.basemvvm.enums;

/**
 * Trạng thái rotuing day
 * 0: đang vận chuyển
 * 1: lái xe đã đến kho
 * 2: đã xuất/nhập hàng
 * 3: đơn bị hủy
 * 4: đơn có thay đổi đang chờ customer duyệt
 */

public class RoutingPlanDayStatus {
    public static final Integer SHIPPING = 0;
    public static final Integer THE_DRIVER_HAS_ARRIVED = 1;
    public static final Integer COMPLETED = 2;
    public static final Integer ORDER_CANCELLED = 3;
    public static final Integer IN_CLAIM = 4;
}
