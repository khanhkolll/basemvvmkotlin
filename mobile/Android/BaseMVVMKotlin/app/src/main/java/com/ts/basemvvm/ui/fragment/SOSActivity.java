package com.ts.basemvvm.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.GridImageAdapter;
import com.ts.basemvvm.adapter.XBaseAdapter;
import com.ts.basemvvm.databinding.SosActivityBinding;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.SOSViewModel;
import com.ts.basemvvm.widget.FullyGridLayoutManager;
import com.ts.basemvvm.widget.GlideEngine;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

public class SOSActivity extends BaseActivity<SosActivityBinding> {
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    private static final int LOCATION_SETTING_CODE = 111;

    SOSViewModel sosViewModel;
    private GridImageAdapter imageSelectAdapter;
    XBaseAdapter sosAdapter;
    private int preCate = -1;
    FusedLocationProviderClient location;
    AlertDialog alert;
    DialogChooseSOSType chooseSOSType;
    private int routing_plan_id = -1;
    private int orderNumber = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sosViewModel = (SOSViewModel) viewModel;
        location = LocationServices.getFusedLocationProviderClient(this);

        sosViewModel.getTypeList(this::runUi);
        initView();
        initToolbar();
        initAdapterImage();
    }


    private void initToolbar() {
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void initView() {
        Intent intent = getIntent();
        routing_plan_id = intent.getIntExtra("ROUTING_PLAN_ID", -1);
        orderNumber = intent.getIntExtra("ORDER_NUMBER", -1);

        sosAdapter = new XBaseAdapter(R.layout.item_sos, sosViewModel.getTypeList(), this);
        binding.rcSOS.setAdapter(sosAdapter);
    }


    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }


    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcSelectImages.setLayoutManager(manager);

        binding.rcSelectImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }

    private void initAdapterImage() {
        initLayoutManager();
        //list select image
        imageSelectAdapter = new GridImageAdapter(this, this::pickImage);
        imageSelectAdapter.setSelectMax(12);
        imageSelectAdapter.setOnClear(() -> {
            sosViewModel.isNotEmptyImage.set(false);
        });
        binding.rcSelectImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });

    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        if (v.getId() == R.id.itemSOS) {
            int position = ((BaseModel) o).index - 1;
            binding.rcSOS.smoothScrollToPosition(position);
            sosViewModel.getTypeList().get(position).checked = true;
            sosAdapter.notifyItemChanged(position);

            if (preCate >= 0 && position != preCate) {
                sosViewModel.getTypeList().get(preCate).checked = false;
                sosAdapter.notifyItemChanged(preCate);
            }
            preCate = position;
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getSOSSuccess":
                runOnUiThread(() -> {
                    sosAdapter.notifyDataSetChanged();
                });
                break;
            case "create_sos_success":
                ToastUtils.showToast(this, getString(R.string.report_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
                setResult(RESULT_OK);
                finish();
                break;
            case "create_sos_fail":
                Toast.makeText(this, R.string.report_sos_fail, Toast.LENGTH_SHORT).show();
                break;
            case "not_on_shipping":
                Toast.makeText(this, R.string.you_are_not_shipping, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void openReport() {
        String address = "";
        if (binding.txtAddress.getText().length() == 0) {
            binding.txtAddress.setError(getString(R.string.please_enter_address));
            return;
        }
        if (preCate >= 0) {
            chooseSOSType = new DialogChooseSOSType((continuable, delayTime) -> {
                onReport(continuable, delayTime, address);
            });
            chooseSOSType.show(getSupportFragmentManager(), chooseSOSType.getTag());
        } else {
            ToastUtils.showToast(this, R.string.please_select_a_sos);
        }

    }

    @SuppressLint("MissingPermission")
    public void onReport(boolean continuable, int delayTime, String address) {
        if (routing_plan_id != -1) {
            location.getLastLocation()
                    .addOnSuccessListener(this, location1 -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location1 != null) {
                            sosViewModel.createSOS(preCate, routing_plan_id
                                    , new LatLng(location1.getLatitude(), location1.getLongitude())
                                    , continuable, delayTime, address, orderNumber
                                    , this::runUi);
                        } else {
                            buildAlertMessageNoGps();
                        }
                    });

        } else {
            ToastUtils.showToast(this, R.string.can_not_get_current_routing);
        }
    }

    private void buildAlertMessageNoGps() {
        if (alert == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.msg_turn_on_gps))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), (dialog, id)
                            -> startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTING_CODE));
            alert = builder.create();
        }
        if (!alert.isShowing()) {
            alert.show();
        }
    }


    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .maxSelectNum(12)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new SOSActivity.MyResultCallback(imageSelectAdapter, sosViewModel));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                ToastUtils.showToast(this, R.string.permissions_not_granted);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private SOSViewModel sosViewModel;

        public MyResultCallback(GridImageAdapter adapter, SOSViewModel sosViewModel) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.sosViewModel = sosViewModel;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            sosViewModel.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }

    }


    @Override
    public int getLayoutRes() {
        return R.layout.sos_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SOSViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
