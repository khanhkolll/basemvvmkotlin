package com.ts.basemvvm.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SizeStandard extends BaseModel {

    private Integer id;
    private Integer length;
    private Integer width;
    private Integer height;
    private String longUnit;

}