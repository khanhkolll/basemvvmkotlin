package com.ts.basemvvm.base;

public interface INotifyDataRouting {
    void notifyData(boolean isChange);
}
