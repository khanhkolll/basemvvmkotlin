package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.ts.basemvvm.api.DriverApi;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.model.Driver;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleHistoryVM extends BaseViewModel {
    private List<Driver> vehicleHistory;
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableInt records = new ObservableInt();

    public VehicleHistoryVM(@NonNull Application application) {
        super(application);
        vehicleHistory = new ArrayList<>();
    }

    public void getVehicleHistory(String date, RunUi runUi){
        isLoading.set(true);
        vehicleHistory.clear();
        DriverApi.getVehicleHistory(date, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                vehicleHistory.addAll((List<Driver>) o);
                records.set(((List<Driver>) o).size());
                runUi.run("getHistorySuccess");
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getHistoryFail");
                isLoading.set(false);
            }
        });
    }


}
