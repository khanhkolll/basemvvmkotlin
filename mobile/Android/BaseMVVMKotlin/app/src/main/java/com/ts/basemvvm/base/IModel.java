package com.ts.basemvvm.base;

public interface IModel {
    String getModelName();
}
