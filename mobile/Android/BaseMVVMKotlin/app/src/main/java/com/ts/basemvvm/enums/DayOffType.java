package com.ts.basemvvm.enums;

public class DayOffType {
    public static String WHOLE_DAY = "1";
    public static String MORNING = "2";
    public static String AFTERNOON = "3";
}
