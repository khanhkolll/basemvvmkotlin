package com.ts.basemvvm.ui.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.ts.basemvvm.R;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.ChooseStatusRoutingDialogBinding;
import com.ts.basemvvm.model.ChooseItemDialogVM;

public class ChooseStatusRoutingDialog extends DialogFragment {

    ChooseItemDialogVM chooseItemDialogVM;
    IGetRadioChecked iGetRadioChecked;
    ChooseStatusRoutingDialogBinding binding;
    int position;

    public ChooseStatusRoutingDialog(int position,IGetRadioChecked iGetRadioChecked) {
        this.position = position;
        this.iGetRadioChecked=iGetRadioChecked;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.choose_status_routing_dialog, container, false);
        chooseItemDialogVM = ViewModelProviders.of(this).get(ChooseItemDialogVM.class);
        binding.setViewModel(chooseItemDialogVM);
        binding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        init();
        binding.btnClose.setOnClickListener(v -> dismiss());
        return binding.getRoot();
    }
    public void onClick(View view){
        switch (view.getId()) {
            case R.id.rbAll:
                iGetRadioChecked.getRadioCheck(0,getString(R.string.ALL));
                break;
            case R.id.rbUnConfirm:
                iGetRadioChecked.getRadioCheck(1,getString(R.string.in_transit));
                break;
            case R.id.rbDriverConfirm:
                iGetRadioChecked.getRadioCheck(2,getString(R.string.driver_confirm));
                break;
            case R.id.rbConfirmAndComplete:
                iGetRadioChecked.getRadioCheck(3,getString(R.string.COMPLETE));
                break;
            case R.id.rbDelete:
                iGetRadioChecked.getRadioCheck(4,getString(R.string.CANCELLED));
                break;
            case R.id.rbWaitConfirm:
                iGetRadioChecked.getRadioCheck(5,getString(R.string.waiting_confirm));
                break;
        }
    }
    public void init() {
        switch (position) {
            case 1:
                binding.rbUnConfirm.setChecked(true);
                break;
            case 2:
                binding.rbDriverConfirm.setChecked(true);
                break;
            case 3:
                binding.rbConfirmAndComplete.setChecked(true);
                break;
            case 4:
                binding.rbDelete.setChecked(true);
                break;
            case 5:
                binding.rbWaitConfirm.setChecked(true);
                break;
            default:
                binding.rbAll.setChecked(true);
                break;
        }
    }


    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            getResult();
            dismiss();
        }
    }


    public void getResult() {
        Intent intent = new Intent();
        intent.putExtra(Constants.SUCCESS_API, Constants.SUCCESS_API);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
        this.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
    public interface IGetRadioChecked{
        void getRadioCheck(int position,String txt);
    }
}
