package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticalChildVM extends BaseViewModel {
    ObservableBoolean isRefresh = new ObservableBoolean();
    ObservableBoolean emptyData= new ObservableBoolean();

    public StatisticalChildVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
    }
}
