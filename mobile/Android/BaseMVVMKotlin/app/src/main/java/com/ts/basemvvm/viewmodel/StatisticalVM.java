package com.ts.basemvvm.viewmodel;

        import android.app.Application;

        import androidx.annotation.NonNull;
        import androidx.databinding.ObservableArrayList;
        import androidx.databinding.ObservableBoolean;
        import androidx.databinding.ObservableList;

        import com.ns.odoolib_retrofit.model.OdooResultDto;
        import com.ns.odoolib_retrofit.utils.OdooDateTime;
        import com.ts.basemvvm.api.DriverApi;
        import com.ts.basemvvm.api.SharingOdooResponse;
        import com.ts.basemvvm.base.Constants;
        import com.ts.basemvvm.base.RunUi;
        import com.ts.basemvvm.model.StatisticalWalletDTO;
        import com.tsolution.base.BaseViewModel;

        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.List;

        import lombok.Getter;
        import lombok.Setter;

@Getter
@Setter
public class StatisticalVM extends BaseViewModel {
    ObservableBoolean isRefresh = new ObservableBoolean();
    ObservableList<StatisticalWalletDTO> statisticalWalletDTOS = new ObservableArrayList<>();
    List<StatisticalWalletDTO> dataTemp = new ArrayList<>();
    ObservableList<String> month_history = new ObservableArrayList<>();
    int year;
    int month;

    public StatisticalVM(@NonNull Application application) {
        super(application);
        initData();
    }

    public void getStatisticalWallet(RunUi runUi) {
        DriverApi.getStatisticalWallet(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<StatisticalWalletDTO> resultDto = (OdooResultDto<StatisticalWalletDTO>) o;
                if (resultDto != null) {
                    getDataSuccess(resultDto.getRecords());
                    runUi.run(Constants.SUCCESS_API);
                } else {
                    runUi.run(Constants.FAIL_API);
                }
            }
            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    //initData on 6 month ago
    private void initData() {
        Date date = new Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        for (int i = 0; i < 6; i++) {
            StatisticalWalletDTO dtoTemp = new StatisticalWalletDTO();
            if ((month - i) < 1) {
                int month = this.month + 12 - i;
                int year = this.year - 1;
                cal.set(year, month, day);
            } else {
                int month = this.month - i;
                cal.set(year, month, day);
            }
            dtoTemp.setMonth(new OdooDateTime(cal.getTime().getTime()));
            dataTemp.add(dtoTemp);
        }
    }

    private void getDataSuccess(List<StatisticalWalletDTO> dtoList) {
        int index=0;
        for (StatisticalWalletDTO item1 : dataTemp) {
            for (StatisticalWalletDTO item : dtoList) {
                if (item.getMonth().getMonth() == item1.getMonth().getMonth()) {
                    dataTemp.set(index,item);
                }
            }
            index++;
        }
        statisticalWalletDTOS.addAll(dataTemp);
    }


}
