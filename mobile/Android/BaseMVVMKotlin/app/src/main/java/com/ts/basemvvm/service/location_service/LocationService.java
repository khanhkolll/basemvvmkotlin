package com.ts.basemvvm.service.location_service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.maps.model.LatLng;
import com.ts.basemvvm.R;
import com.ts.basemvvm.api.DriverApi;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.model.DriverDistance;

/**
 * @author Good_Boy
 * Service này dùng để kiểm tra nếu xe gần đến kho xe bắn thông báo đến nhà kho hoặc khách hàng
 * để chuẩn bị đón xe.
 * routing_id: id của tuyến
 * interval: khoảng thời gian để kiểm tra - đơn vị giây
 * distance: khoảng cách tới kho hàng sẽ bắn thông báo - đơn vị m
 * minute: khoảng thời gian tới kho - đơn vị phút.
 * lat, lng : tọa độ của kho hàng đang đến.
 */
public class LocationService extends Service implements PositionProvider.PositionListener {
    Context context;
    private PositionProvider positionProvider;

    int routing_id;
    int interval;
    int distance;
    int minute;
    double lat;
    double lng;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        positionProvider = new AndroidPositionProvider(context, this);

    }

    @Override
    public void onPositionUpdate(Position position) {
        Log.e("POSITION", "lat:" + position.getLatitude() + ", lng:" + position.getLongitude());
        LatLng from = new LatLng(position.getLatitude(), position.getLongitude());
        LatLng to = new LatLng(lat, lng);
        DriverApi.getDirection(from, to, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o == null) return;
                DriverDistance driverDistance = (DriverDistance) o;
                //call api thông báo.
                if (driverDistance.getCost() <= distance || driverDistance.getMinutes() <= minute) {
                    DriverApi.acceptComing(routing_id, new IResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            //200: gửi thành công , 201: đã gửi rồi.
                            if (o.equals(200) || o.equals(201)) {
                                stopLocationService();
                            }
                        }

                        @Override
                        public void onFail(Throwable error) {

                        }
                    });
                }
            }

            @Override
            public void onFail(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onPositionError(Throwable error) {

    }

    private void startLocationService() {

        String channelId = "location_service_channel";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent resultIntent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelId);
        builder.setSmallIcon(R.drawable.ic_location);
        builder.setContentTitle(getString(R.string.location_service));
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        builder.setContentText(getString(R.string.running));
        builder.setAutoCancel(false);
        builder.setContentIntent(pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            if (notificationManager != null
                    && notificationManager.getNotificationChannel(channelId) == null) {
                NotificationChannel channel = new NotificationChannel(channelId, "Location service",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(getString(R.string.this_channel_use_by_location_service));
                notificationManager.createNotificationChannel(channel);
            }
        }

        positionProvider.setConfig(routing_id + "", interval, 0, 0);
        Log.e("POSITION", "START");
        positionProvider.startUpdates();

        startForeground(Constants.LOCATION_SERVICE_ID, builder.build());
    }

    private void stopLocationService() {
        stopForeground(true);
        positionProvider.stopUpdates();
        Log.e("POSITION", "STOP");
        stopSelf();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int ONE_SECOND = 1000;
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(Constants.ACTION_START_LOCATION_SERVICE)) {
                    int interval = intent.getIntExtra(Constants.INTERVAL, 60) * ONE_SECOND;
                    int routing_id = intent.getIntExtra(Constants.ROUTING_ID, -1);
                    int distance = intent.getIntExtra(Constants.DISTANCE, 3000);
                    int minute = intent.getIntExtra(Constants.MINUTE, 35);
                    double lat = intent.getDoubleExtra(Constants.LATITUDE, 0);
                    double lng = intent.getDoubleExtra(Constants.LONGITUDE, 0);
                    setConfig(routing_id, interval, distance, minute, lat, lng);

                    startLocationService();

                } else if (action.equals(Constants.ACTION_STOP_LOCATION_SERVICE)) {
                    stopLocationService();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);

    }

    private void setConfig(int routing_id, int interval, int distance, int minute, double lat, double lng) {
        this.routing_id = routing_id;
        this.interval = interval;
        this.distance = distance;
        this.minute = minute;
        this.lat = lat;
        this.lng = lng;
    }
}
