package com.ts.basemvvm.adapter;

import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseNodeAdapter;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.provider.FooterNodeProvider;
import com.ts.basemvvm.adapter.provider.HeaderNodeProvider;
import com.ts.basemvvm.adapter.provider.SecondRoutingNodeProvider;
import com.ts.basemvvm.model.section.ItemRoutingNote;
import com.ts.basemvvm.model.section.RootFooterNode;
import com.ts.basemvvm.model.section.RootNode;
import com.tsolution.base.BaseViewModel;


import java.util.List;

import androidx.annotation.NonNull;

public class RoutingNodeAdapter extends BaseNodeAdapter {

    public static final int EXPAND_COLLAPSE_PAYLOAD = 110;

    public RoutingNodeAdapter(BaseViewModel baseViewModel) {
        super();
        addFullSpanNodeProvider(new HeaderNodeProvider(R.layout.routing_root_node,baseViewModel));
        addNodeProvider(new SecondRoutingNodeProvider(R.layout.item_routing_note,baseViewModel));
        addFooterNodeProvider(new FooterNodeProvider(R.layout.routing_selection_footer));
    }

    @NonNull
    @Override
    protected BaseViewHolder onCreateDefViewHolder(@NonNull ViewGroup parent, int viewType) {
        return super.onCreateDefViewHolder(parent, viewType);

    }

    @Override
    protected int getItemType(@NonNull List<? extends BaseNode> data, int position) {
        BaseNode node = data.get(position);
        if (node instanceof RootNode) {
            return 0;
        } else if (node instanceof ItemRoutingNote) {
            return 1;
        } else if (node instanceof RootFooterNode) {
            return 2;
        }
        return -1;
    }
}
