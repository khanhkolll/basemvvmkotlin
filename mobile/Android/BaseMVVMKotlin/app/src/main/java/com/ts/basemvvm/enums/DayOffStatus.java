package com.ts.basemvvm.enums;

public class DayOffStatus {
    public static String WAITING = "1";
    public static String ACCEPTED = "2";
    public static String DENIED = "3";
    public static String CANCELED = "4";
}
