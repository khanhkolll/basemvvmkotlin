package com.ts.basemvvm.model.section;

import com.chad.library.adapter.base.entity.node.BaseNode;


import java.util.List;

import androidx.annotation.Nullable;

public class RootFooterNode extends BaseNode {

    private String title;
    private List<BaseNode> lstChild;

    public RootFooterNode(String title, List<BaseNode> baseNodes) {
        this.title = title;
        this.lstChild = baseNodes;
    }

    public RootFooterNode(String title) {
        this.title = title;
    }

    public List<BaseNode> getLstChild() {
        return lstChild;
    }

    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public List<BaseNode> getChildNode() {
        return null;
    }
}
