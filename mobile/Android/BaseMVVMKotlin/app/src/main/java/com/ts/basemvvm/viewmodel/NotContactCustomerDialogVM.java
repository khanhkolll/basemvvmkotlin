package com.ts.basemvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotContactCustomerDialogVM extends BaseViewModel {
    private List<String> listImage= new ArrayList<>();
    private List<String> listUrl= new ArrayList<>();
    private ObservableBoolean isNotEmptyImage = new ObservableBoolean(false);
    private ObservableBoolean isChooseDate = new ObservableBoolean(false);
    public NotContactCustomerDialogVM(@NonNull Application application) {
        super(application);
    }
}
