package com.ts.basemvvm.model;

import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.basemvvm.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class  ShareVanRoutingPlan extends BaseModel {
    private Integer id;
    private Integer bill_lading_detail_id;
    private Integer bill_routing_id;
    private String bill_routing_name;
    private String routing_plan_day_code;
    private Integer order_number;
    private Integer company_id;
    private Integer warehouse_id;
    private String address;
    private String warehouse_name;
    private String phone;
    private String qr_code;
    private String bill_lading_detail_code;
    private boolean check_point;//đã thông báo sắp đến chưa
    private boolean toogle;//đã thông báo sắp đến chưa
    private boolean arrived_check;//đã đến kho chưa
    private boolean first_rating_customer;
    private int total_qr_check;// Số lượng mã qr phải check
    private int total_qr_checked;// Số lượng mã qr đã check
    private ShareVanRoutingPlan warehouse_return;

    //rating_customer : true(đã đánh giá), false(chưa đánh giá)
    private boolean rating_customer;
    private RatingCustomerDTO rating_customer_id;
    /*
    [('0','Chưa xác nhận'),
    ('1','Lái xe xác nhận'),
    ('2','Đã xác nhận')
    ('3','Đã hủy')
    ('4','Chỉnh sửa')]
    */
    private Integer status;

    private String type;// 0:trả hàng, 1: nhận hàng.
    private String trouble_type;

    private OdooDateTime expected_from_time;
    private OdooDateTime expected_to_time;
    private OdooDate date_plan;

    private Float latitude;
    private Float longitude;

    private OdooDateTime accept_time;// time customer confirm
    private OdooDateTime confirm_time;// time driver confirm

    private Boolean qr_gen_check; // gen qr code

    private Vehicle vehicle;
    private Driver driver;

    private List<String> image_urls;//ảnh của thay đổi
    private String description;//thông tin mô tẩ chi tiết thay đổi

    private String ship_type;
    private String order_type;//0: trong zone, 1: ngoài zone  - so với kho xuất hàng.
    private Boolean so_type; // check đơn so
    private String qr_so;
    private String insurance_name;
    private List<BillService> services;
    List<BillPackage> list_bill_package;


    public String getConfirmTimeStr(){
        if(this.confirm_time == null){
            return "";
        }
        return AppController.formatDateTime.format(confirm_time);
    }

    public String getDatePlanStr(){
        if(this.date_plan == null){
            return "";
        }
        return AppController.formatDate.format(date_plan);
    }

    public String getExpectedFromTimeStr(){
        if(this.expected_from_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_from_time);
    }

    public String getExpectedToTimeStr(){
        if(this.expected_to_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_to_time);
    }

}
