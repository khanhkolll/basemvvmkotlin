package com.ts.basemvvm.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MySubscriber extends BaseModel {
    private Long id;
    private int type;
    private String description;
}
