package com.ts.basemvvm.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.ActivityWorkingScheduleBinding;
import com.ts.basemvvm.enums.DayOffStatus;
import com.ts.basemvvm.enums.DayOffType;
import com.ts.basemvvm.model.DayOff;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.WorkingScheduleVM;
import com.ts.basemvvm.widget.CustomMultiMonthView;
import com.ts.basemvvm.widget.CustomSingleMonthView;
import com.ts.basemvvm.widget.OnSingleClickListener;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.Date;
import java.util.List;

public class WorkingScheduleActivity extends BaseActivity<ActivityWorkingScheduleBinding> implements
        CalendarView.OnCalendarRangeSelectListener,
        CalendarView.OnCalendarInterceptListener,
        CalendarView.OnYearChangeListener,
        CalendarView.OnMonthChangeListener,
        CalendarView.OnCalendarLongClickListener {

    Calendar currentDate = new Calendar();
    private int mYear;
    private int mMonth;
    private WorkingScheduleVM workingScheduleVM;
    Long end_date=0l;
    Long start_date=0l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        workingScheduleVM = (WorkingScheduleVM) viewModel;

        initToolbar();
        initView();
        initData();
    }


    @SuppressLint("SetTextI18n")
    private void initToolbar() {
        binding.btnBack.setOnClickListener(v -> {
            if (binding.calendar.isYearSelectLayoutVisible()) {
                binding.calendar.closeYearSelectLayout();
            } else {
                finish();
            }
        });
        binding.tvMonthDay.setOnClickListener(v -> {
            binding.calendar.showYearSelectLayout(mYear);
            binding.tvYear.setVisibility(View.GONE);
            binding.tvMonthDay.setText(String.valueOf(mYear));
        });
        binding.flCurrent.setOnClickListener(v -> binding.calendar.scrollToCurrent());
        binding.tvYear.setText(String.valueOf(binding.calendar.getCurYear()));
        mYear = binding.calendar.getCurYear();
        mMonth = binding.calendar.getCurMonth();

        currentDate.setDay(binding.calendar.getCurDay());
        currentDate.setMonth(binding.calendar.getCurMonth());
        currentDate.setYear(binding.calendar.getCurYear());

        String monthStr = mMonth + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        String dayStr = binding.calendar.getCurDay() + "";
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        binding.tvMonthDay.setText(dayStr + " / " + monthStr);
        binding.tvCurrentDay.setText(String.valueOf(binding.calendar.getCurDay()));
    }

    private void initView() {
        Intent intent= getIntent();
        if(intent.hasExtra(Constants.FROM_FRAGMENT)){
            String dateStr = intent.getStringExtra(Constants.DATA);
        }
        Calendar calendar = new Calendar();
        calendar.setDay(java.util.Calendar.getInstance().get(java.util.Calendar.DAY_OF_YEAR)+1);
        calendar.setMonth(java.util.Calendar.getInstance().get(java.util.Calendar.MONTH));
        calendar.setYear(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
        onCalendarRangeSelect(calendar, false);
        binding.calendar.setSelectEndCalendar(calendar);
        workingScheduleVM.setMsg_invalid(getString(R.string.INVALID_FIELD));

        binding.calendar.setOnYearChangeListener(this);
        binding.calendar.setOnCalendarRangeSelectListener(this);
        binding.calendar.setOnCalendarInterceptListener(this);
        binding.calendar.setOnCalendarLongClickListener(this);
        binding.calendar.setOnMonthChangeListener(this);
        binding.calendarLayout.setModeOnlyMonthView();
        binding.calendar.setSchemeDate(workingScheduleVM.getMScheme());

        binding.btnAddDateOff.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                binding.calendar.setMonthView(CustomMultiMonthView.class);
                workingScheduleVM.getIsRangeSelect().set(true);
                binding.calendar.clearSelectRange();
            }
        });

        binding.btnDismiss.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                binding.calendar.setMonthView(CustomSingleMonthView.class);
                workingScheduleVM.getIsRangeSelect().set(false);

                workingScheduleVM.getReason().set("");
                binding.calendar.clearSelectRange();
                workingScheduleVM.getEndDate().set(new DayOff());
                workingScheduleVM.getStartDate().set(new DayOff());
            }
        });

        binding.btnRequest.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (workingScheduleVM.getStartDate().get().getCalendar() == null) {
                    binding.lbErrorStart.setVisibility(View.VISIBLE);
                    return;
                }
                workingScheduleVM.requestDayOff(WorkingScheduleActivity.this::runUi);
            }
        });

        binding.btnCancelDayOff.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                workingScheduleVM.cancelDayOff(WorkingScheduleActivity.this::runUi);
            }
        });
        binding.btnCancelDayOff2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                workingScheduleVM.cancelDayOff(WorkingScheduleActivity.this::runUi);
            }
        });

    }

    protected void initData() {
        workingScheduleVM.getListOff(mMonth, mYear, this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getListDayOffSuccess":
                binding.calendar.update();
                Intent intent= getIntent();
                if(intent.hasExtra(Constants.FROM_FRAGMENT)){
                    String dateStr = intent.getStringExtra(Constants.DATA);
                }
                Calendar calendar = new Calendar();
                calendar.setDay(java.util.Calendar.getInstance().get(java.util.Calendar.DAY_OF_YEAR)+1);
                calendar.setMonth(java.util.Calendar.getInstance().get(java.util.Calendar.MONTH));
                calendar.setYear(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
                onCalendarRangeSelect(calendar, false);
                break;
            case "getListDayOffFail":
                break;
            case "requestSuccess":
                workingScheduleVM.getIsRangeSelect().set(false);
                binding.calendar.update();
                onCalendarRangeSelect(workingScheduleVM.getStartDate().get().getCalendar(), false);

                binding.calendar.setMonthView(CustomSingleMonthView.class);

                workingScheduleVM.getReason().set("");

                workingScheduleVM.getEndDate().set(new DayOff());
                workingScheduleVM.getStartDate().set(new DayOff());

                ToastUtils.showToast(this, getString(R.string.request_success_wait_for_appro), getResources().getDrawable(R.drawable.ic_check));
                break;
            case "requestFail":
                ToastUtils.showToast(this, getString(R.string.request_fail), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
            case "existDayOff":
                binding.calendar.clearSelectRange();
                workingScheduleVM.getEndDate().set(new DayOff());
                workingScheduleVM.getStartDate().set(new DayOff());
                ToastUtils.showToast(this, getString(R.string.msg_exits_day_off_between_start_end), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
            case "cancelSuccess":
                binding.calendar.update();
                onCalendarRangeSelect(workingScheduleVM.getModelE().getCalendar(), false);
                ToastUtils.showToast(this, getString(R.string.cancel_success), getResources().getDrawable(R.drawable.ic_check));
                break;
            case "cancelFail":
                ToastUtils.showToast(this, getString(R.string.cancel_day_off_fail), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
        }
    }


    public void scrollToCurrent() {
        binding.calendar.scrollToCurrent();
    }

    @SuppressLint("NonConstantResourceId")
    public void onCheckDayOffType(RadioGroup rg, int id) {
        switch (id) {
            case R.id.startWholeDay:
                workingScheduleVM.getStartDate().get().setType(DayOffType.WHOLE_DAY);
                break;
            case R.id.endWholeDay:
                workingScheduleVM.getEndDate().get().setType(DayOffType.WHOLE_DAY);
                break;
            case R.id.startMorning:
                workingScheduleVM.getStartDate().get().setType(DayOffType.MORNING);
                break;
            case R.id.startAfternoon:
                workingScheduleVM.getStartDate().get().setType(DayOffType.AFTERNOON);
                break;
            case R.id.endMorning:
                workingScheduleVM.getEndDate().get().setType(DayOffType.MORNING);
                break;
        }
    }

    @Override
    public void onCalendarSelectOutOfRange(Calendar calendar) {
    }

    @Override
    public void onSelectOutOfRange(Calendar calendar, boolean isOutOfMinRange) {

    }

    Calendar startSelect;
    int count = 0;//biến để break vì onCalendarRangSelect sẽ được gọi lại khi hàm setRangeSelect dc gọi

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarRangeSelect(Calendar calendar, boolean isEnd) {
        binding.tvYear.setVisibility(View.VISIBLE);
        String monthStr = calendar.getMonth() + "";
        String dayStr = calendar.getDay() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        binding.tvMonthDay.setText(dayStr + " / " + monthStr);
        binding.tvYear.setText(String.valueOf(calendar.getYear()));
        mYear = calendar.getYear();
        mMonth = calendar.getMonth();


        if (!workingScheduleVM.getIsRangeSelect().get()) {//nếu ko phải là đăng kí ngày nghỉ.
            if (count > 0) {
                count = 0;
                return;
            }
            if (!isEnd) {//nếu là chọn start thì
                count++;
                List<Calendar> selected = workingScheduleVM.getGroupDayOff(calendar);
                //lúc nào cũng có it nhất 1 phần tử.
                Calendar start = selected.get(0);
                startSelect = calendar;
                Calendar end = selected.get(selected.size() - 1);
                end_date=end.getTimeInMillis();
                start_date=end.getTimeInMillis();
                binding.calendar.setSelectCalendarRange(start, end);
            } else {
                //nếu end = start thì gọi hàm chọn để chọn start.
                if (startSelect.hasScheme() && calendar.hasScheme()) {
                    DayOff start = workingScheduleVM.getMapDayOff().get(startSelect.toString());
                    DayOff end = workingScheduleVM.getMapDayOff().get(calendar.toString());
                    start_date = start.getCalendar().getTimeInMillis();
                    //nếu group_id của start date và end khác nhau. (Tính cả gốc lẫn other part) thì gọi hàm chọn start.Tức là start và end nó chả liên quan mẹ gì đến nhau thì chọn lại start khác.
                    if (start.getOtherPart() != null && end.getOtherPart() != null) {
                        if (!(start.getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme())// phủ đinh của : group id của start == end
                                || start.getOtherPart().getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme())//other start == end
                                || start.getCalendar().getSchemes().get(2).getScheme().equals(end.getOtherPart().getCalendar().getSchemes().get(2).getScheme())// start == other end
                                || start.getOtherPart().getCalendar().getSchemes().get(2).getScheme().equals(end.getOtherPart().getCalendar().getSchemes().get(2).getScheme()))) {//other start == other end
                            binding.calendar.setSelectCalendarRange(calendar, calendar);//gọi lại hàm chọn để chọn sang vùng mới
                        }
                    } else if (start.getOtherPart() != null && end.getOtherPart() == null) {
                        if (!(start.getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme())//phủ định của: group id của start == end
                                || start.getOtherPart().getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme()))) {//other start == end
                            binding.calendar.setSelectCalendarRange(calendar, calendar);
                        }
                    } else if (start.getOtherPart() == null && end.getOtherPart() != null) {
                        if (!(start.getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme())//start == end
                                || start.getCalendar().getSchemes().get(2).getScheme().equals(end.getOtherPart().getCalendar().getSchemes().get(2).getScheme()))) {// start == other end
                            binding.calendar.setSelectCalendarRange(calendar, calendar);
                        }
                    } else {
                        if (!(start.getCalendar().getSchemes().get(2).getScheme().equals(end.getCalendar().getSchemes().get(2).getScheme()))) {//start != end
                            binding.calendar.setSelectCalendarRange(calendar, calendar);
                        }
                    }
                } else {
                    binding.calendar.setSelectCalendarRange(calendar, calendar);
                }
            }
            // Ngày cuối nhỏ hơn ngày hiện tại thì ẩn nút hủy yêu cầu
            if (end_date < (new Date()).getTime()) {
                workingScheduleVM.getIsCancel().set(false);
            } else {
                workingScheduleVM.getIsCancel().set(true);
            }
        } else {// xử lý thêm ngày nghỉ.
            DayOff exits = workingScheduleVM.getMapDayOff().get(calendar.toString());
            boolean enable = true;//biến cho phép tạo lịch nghỉ
            if (!isEnd) {
                binding.lbErrorStart.setVisibility(View.GONE);

                workingScheduleVM.getStartDate().get().setCalendarValue(calendar);
                workingScheduleVM.getStartDate().notifyChange();

                binding.existStartDate.setVisibility(View.GONE);

                if (workingScheduleVM.getEndDate().get().getCalendar() != null) {
                    workingScheduleVM.getEndDate().set(new DayOff());
                }

                //check nếu ngày bắt đầu tồn tại trong mapDayOff
                if (exits != null) {
                    if (exits.getOtherPart() == null) {//kiểm tra nếu ko có thành phân khác trong ngày.
                        if (exits.getType().equals(DayOffType.MORNING)) {//nếu loại là nghỉ sáng.
                            if (exits.getStatus().equals(DayOffStatus.CANCELED)) {//nếu trạng thái là tự hủy
                                visibleStartAll();
                            } else {
                                binding.startWholeDay.setVisibility(View.GONE);
                                binding.startAfternoon.setVisibility(View.VISIBLE);
                                binding.startMorning.setVisibility(View.GONE);
                                binding.startAfternoon.setChecked(true);
                            }

                        } else if (exits.getType().equals(DayOffType.AFTERNOON)) {
                            if (exits.getStatus().equals(DayOffStatus.CANCELED)) {
                                visibleStartAll();
                            } else {
                                binding.startWholeDay.setVisibility(View.GONE);
                                binding.startAfternoon.setVisibility(View.GONE);
                                binding.startMorning.setVisibility(View.VISIBLE);
                                binding.startMorning.setChecked(true);
                            }
                        } else {
                            visibleStartAll();
                        }
                    } else {
                        binding.startWholeDay.setVisibility(View.GONE);
                        if (exits.getStatus().equals(DayOffStatus.CANCELED) && !exits.getOtherPart().getStatus().equals(DayOffStatus.CANCELED)) {
                            if (exits.getType().equals(DayOffType.MORNING)) {
                                binding.startAfternoon.setVisibility(View.GONE);
                                binding.startMorning.setVisibility(View.VISIBLE);
                                binding.startMorning.setChecked(true);
                            } else {
                                binding.startAfternoon.setVisibility(View.VISIBLE);
                                binding.startMorning.setVisibility(View.GONE);
                                binding.startAfternoon.setChecked(true);
                            }
                        } else if (exits.getOtherPart().getStatus().equals(DayOffStatus.CANCELED) && !exits.getStatus().equals(DayOffStatus.CANCELED)) {
                            if (exits.getOtherPart().getType().equals(DayOffType.MORNING)) {
                                binding.startAfternoon.setVisibility(View.GONE);
                                binding.startMorning.setVisibility(View.VISIBLE);
                                binding.startMorning.setChecked(true);
                            } else {
                                binding.startAfternoon.setVisibility(View.VISIBLE);
                                binding.startMorning.setVisibility(View.GONE);
                                binding.startAfternoon.setChecked(true);
                            }
                        } else {
                            visibleStartAll();
                        }
                    }
                } else {
                    visibleStartAll();
                }

            } else {
                //check ngày bắt đầu khi chọn ngày kết thúc
                DayOff startExits = workingScheduleVM.getMapDayOff().get(workingScheduleVM.getStartDate().get().getCalendar().toString());
                if (startExits != null) {
                    if (DayOffType.AFTERNOON.equals(startExits.getType()) && !startExits.getStatus().equals(DayOffStatus.CANCELED)) {
                        binding.existStartDate.setVisibility(View.VISIBLE);
                        enable = false;
                    } else if (startExits.getOtherPart() != null) {
                        if (DayOffType.AFTERNOON.equals(startExits.getOtherPart().getType()) && !startExits.getOtherPart().getStatus().equals(DayOffStatus.CANCELED)) {
                            binding.existStartDate.setVisibility(View.VISIBLE);
                            enable = false;
                        }
                    }
                } else {
                    binding.existStartDate.setVisibility(View.GONE);
                }

                //check nếu ngày kết thúc tồn tại trong mapDayOff
                if (exits != null) {
                    if (exits.getOtherPart() == null) {
                        if (exits.getStatus().equals(DayOffStatus.CANCELED)) {//nếu trạng thái là tự hủy
                            visibleEndAll();
                        } else {
                            binding.endWholeDay.setVisibility(View.GONE);
                            if (exits.getType().equals(DayOffType.MORNING)) {//nếu loại là nghỉ sáng.
                                binding.endMorning.setVisibility(View.GONE);
                                enable = false;
                                binding.existEndDate.setVisibility(View.VISIBLE);
                            } else {
                                binding.existEndDate.setVisibility(View.GONE);
                                binding.endMorning.setVisibility(View.VISIBLE);
                                binding.endMorning.setChecked(true);
                            }
                        }

                    } else {
                        binding.endWholeDay.setVisibility(View.GONE);
                        if (DayOffType.MORNING.equals(exits.getType())) {
                            binding.endMorning.setVisibility(View.GONE);
                            enable = false;
                            binding.existEndDate.setVisibility(View.VISIBLE);
                        } else {
                            binding.existEndDate.setVisibility(View.GONE);
                            binding.endMorning.setVisibility(View.VISIBLE);
                            binding.endMorning.setChecked(true);
                        }
                    }
                } else {
                    visibleEndAll();
                }

                workingScheduleVM.getEndDate().get().setCalendarValue(calendar);
                workingScheduleVM.getEndDate().notifyChange();
            }
            binding.btnRequest.setEnabled(enable);
        }

    }

    private void visibleEndAll() {
        binding.endWholeDay.setChecked(true);
        binding.existEndDate.setVisibility(View.GONE);
        binding.endMorning.setVisibility(View.VISIBLE);
        binding.endWholeDay.setVisibility(View.VISIBLE);
    }

    private void visibleStartAll() {
        binding.startAfternoon.setVisibility(View.VISIBLE);
        binding.startMorning.setVisibility(View.VISIBLE);
        binding.startWholeDay.setVisibility(View.VISIBLE);
        binding.startWholeDay.setChecked(true);
    }

    public void setViewPart(boolean isShowMorning) {
        workingScheduleVM.getIsViewMorning().set(isShowMorning);
    }


    @Override
    public boolean onCalendarIntercept(Calendar calendar) {
        boolean disable = false;
        if (calendar.hasScheme()) {
            DayOff dayOff = workingScheduleVM.getMapDayOff().get(calendar.toString());
            if (dayOff != null) {
                if (DayOffType.WHOLE_DAY.equals(dayOff.getType())) {
                    if (!DayOffStatus.CANCELED.equals(dayOff.getStatus())) {
                        disable = true;
                    }
                } else {
                    if (dayOff.getOtherPart() != null) {
                        if (!DayOffStatus.CANCELED.equals(dayOff.getStatus()) && !DayOffStatus.CANCELED.equals(dayOff.getOtherPart().getStatus())) {
                            disable = true;
                        }
                    }
                }
            }

        }

        if (calendar.differ(currentDate) <= 0) {
            disable = true;
        }

        return workingScheduleVM.getIsRangeSelect().get() && disable;
    }

    @Override
    public void onCalendarInterceptClick(Calendar calendar, boolean isClick) {

    }

    @Override
    public void onMonthChange(int year, int month) {
        String monthStr;
        if (month < 10) {
            monthStr = "0" + month;
        } else {
            monthStr = month + "";
        }
        binding.tvMonthDay.setText(monthStr);
        binding.tvYear.setText(String.valueOf(year));
        workingScheduleVM.getListOff(month, year, this::runUi);
    }

    @Override
    public void onYearChange(int year) {
        binding.tvMonthDay.setText(String.valueOf(year));
        binding.tvYear.setText("");

    }

    @Override
    public void onBackPressed() {
        if (binding.calendar.isYearSelectLayoutVisible()) {
            binding.calendar.closeYearSelectLayout();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_working_schedule;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WorkingScheduleVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    @Override
    public void onCalendarLongClickOutOfRange(Calendar calendar) {
        ToastUtils.showToast("onLongClick outOfRange: " + calendar.getDay());
    }

    @Override
    public void onCalendarLongClick(Calendar calendar) {
        ToastUtils.showToast("onLongClick: " + calendar.getDay());
    }
}