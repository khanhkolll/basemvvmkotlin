/*
 * Copyright 2013 - 2019 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ts.basemvvm.service.location_service;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.BatteryManager;
import android.util.Log;

public abstract class PositionProvider {

    private static final String TAG = PositionProvider.class.getSimpleName();

    protected static final int MINIMUM_INTERVAL = 1000;

    public interface PositionListener {

        void onPositionUpdate(Position position);

        void onPositionError(Throwable error);
    }

    protected final PositionListener listener;

    protected final Context context;

    protected String deviceId;
    protected long interval;
    protected double distance;
    protected double angle;
    protected Location lastLocation;

    public PositionProvider(Context context, PositionListener listener) {
        this.context = context;
        this.listener = listener;

        //thay băng vehicle id
        deviceId = "62922";
        interval = 30 * 1000;
        distance = 0;
        angle = 0;
    }

    /**
     *
     * @param deviceId mã thiết bị
     * @param interval khoảng thời gian lấy vị trí - đơn vị giấy.
     * @param distance khoảng cách lấy vị trí - đơn vị m.
     * @param angle góc lấy vị trí.
     */
    public void setConfig(String deviceId, long interval, double distance, double angle){
        this.deviceId = deviceId;
        this.interval = interval;
        this.distance = distance;
        this.angle = angle;
    }

    public abstract void startUpdates();

    public abstract void stopUpdates();

    public abstract void requestSingleLocation();

    protected void processLocation(Location location) {
        if (location != null) {
            if (lastLocation == null
                    || location.getTime() - lastLocation.getTime() >= interval
                    || distance > 0 && location.distanceTo(lastLocation) >= distance
                    || angle > 0 && Math.abs(location.getBearing() - lastLocation.getBearing()) >= angle) {
                lastLocation = location;
                listener.onPositionUpdate(new Position(deviceId, location, getBatteryLevel(context)));
            }
            else {
                Log.i(TAG, "location ignored");
            }
        } else {
            Log.i(TAG, "location nil");
        }
    }

    protected static double getBatteryLevel(Context context) {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        if (batteryIntent != null) {
            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);
            return (level * 100.0) / scale;
        }
        return 0;
    }

}
