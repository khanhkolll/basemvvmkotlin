package com.ts.basemvvm.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.basemvvm.base.AppController;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignationLog extends BaseModel {
    private Integer id;
    private String assignation_log_code;
    private OdooDateTime date_start;
    private OdooDateTime date_end;
    private OdooDateTime receive_car;
    private OdooDateTime give_car_back;

    public String getReceive_car_str(){
        if(receive_car == null) return "";
        return AppController.formatDateTime.format(receive_car);
    }

    public String getGive_car_back_str(){
        if(give_car_back == null) return "";
        return AppController.formatDateTime.format(give_car_back);
    }
}
