package com.ts.basemvvm.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import com.haibin.calendarview.Calendar;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.basemvvm.api.DayOffApi;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.enums.DayOffStatus;
import com.ts.basemvvm.enums.DayOffType;
import com.ts.basemvvm.model.DayOff;
import com.ts.basemvvm.utils.TsUtils;
import com.tsolution.base.BaseViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkingScheduleVM extends BaseViewModel<DayOff> {
    private String msg_invalid;

    private HashMap<String, Calendar> mScheme;//map những ngày nghỉ để view
    private HashMap<String, DayOff> mapDayOff;//map những ngày nghỉ từ server trả về
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableBoolean isCancel = new ObservableBoolean();// check ngày chọn và ngày hiện tại
    private ObservableBoolean isOutOfDate = new ObservableBoolean();// check ngày start có phải ngày quá khứ

    private ObservableBoolean isRangeSelect = new ObservableBoolean();// có phải chọn nhiều ngày ko

    //field cho trường hợp chọn tạo ngày nghỉ
    private ObservableField<DayOff> startDate = new ObservableField<>();//ngày bắt đầu - nếu chọn nhiều ngày
    private ObservableField<DayOff> endDate = new ObservableField<>();//ngày kết thúc
    private ObservableField<String> reason = new ObservableField<>("");//lý do nghỉ - app dụng cho selectedDate

    //field cho trường hợp chọn xem ngày nghỉ
    private ObservableField<DayOff> _1stPartStart = new ObservableField<>();
    private ObservableField<DayOff> _1stPartEnd = new ObservableField<>();
    private ObservableField<DayOff> _2ndPartStart = new ObservableField<>();
    private ObservableField<DayOff> _2ndPartEnd = new ObservableField<>();
    private ObservableBoolean isViewMorning = new ObservableBoolean(true);

    private final int color_pending = 0xFFF6AF36;
    private final int color_accept = 0xFF289767;
    private final int color_denied_cancel = 0xFFFF0C20;
    private int color_normal = 0x90CfCfCf;

    public WorkingScheduleVM(@NonNull Application application) {
        super(application);
        mScheme = new HashMap<>(0);
        mapDayOff = new HashMap<>(0);
        startDate.set(new DayOff());
        endDate.set(new DayOff());
        isCancel.set(false);
    }

    public void getListOff(Integer month, Integer year, RunUi runUi) {
        isLoading.set(true);
        DayOffApi.getListDayOff(month, year, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                dumpData((List<DayOff>) o, true);
                isLoading.set(false);
                if (mapDayOff.get(startDate.get().getDateFormatted()) != null) {
                    startDate.set(new DayOff());
                    endDate.set(new DayOff());
                } else if (mapDayOff.get(endDate.get().getDateFormatted()) != null) {
                    endDate.set(new DayOff());
                }
                runUi.run("getListDayOffSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getListDayOffFail");
            }
        });
    }

    private void dumpData(List<DayOff> result, boolean isClear) {
        if (isClear) {
            mScheme.clear();
            mapDayOff.clear();
        }
        for (DayOff temp : result) {
            if (temp.getRequest_day() == null) {
                Log.e("DAY_OFF", temp.getCalendarStr() + "request day = null");
                continue;
            }
            Calendar calendar;//để vẽ

            //nếu có 2 ngày trung nhau
            DayOff exits = mapDayOff.get((temp.getDateFormatted()));
            if (exits != null) {
                exits.setOtherPart(temp);
                calendar = getSchemeCalendar(exits);
                exits.getOtherPart().setCalendar(getSchemeCalendar(temp));
            } else {
                calendar = getSchemeCalendar(temp);
                temp.setCalendar(calendar);
                mapDayOff.put(temp.getDateFormatted(), temp);
            }
            mScheme.put(calendar.toString(), calendar);
        }

    }


    private Calendar getSchemeCalendar(DayOff dayOff) {
        java.util.Calendar cal = new GregorianCalendar();
        cal.setTime(dayOff.getRequest_day());
        int year = cal.get(java.util.Calendar.YEAR);
        int month = cal.get(java.util.Calendar.MONTH) + 1;
        int day = cal.get(java.util.Calendar.DAY_OF_MONTH);

        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);

        if (dayOff.getType().equals(DayOffType.WHOLE_DAY)) {//nếu nghỉ cả ngày -> không có otherPart
            calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());
            calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());
        } else if (dayOff.getOtherPart() != null) {//nếu ko nghỉ cả ngày và có xin nghỉ vào buổi khác
            if (dayOff.getType().equals(DayOffType.AFTERNOON)) {//nếu nghỉ chiều
                calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());//vẽ màu bên trái
                calendar.addScheme(getColor(dayOff.getOtherPart().getStatus()), dayOff.getType());//vẽ màu bên phải
            } else {
                calendar.addScheme(getColor(dayOff.getOtherPart().getStatus()), dayOff.getType());
                calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());
            }
        } else {//nếu ko nghỉ cả ngày và ko xin nghỉ buổi khác
            if (dayOff.getType().equals(DayOffType.AFTERNOON)) {
                calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());
                calendar.addScheme(color_normal, dayOff.getType());
            } else {
                calendar.addScheme(color_normal, dayOff.getType());
                calendar.addScheme(getColor(dayOff.getStatus()), dayOff.getType());
            }
        }

        calendar.addScheme(color_normal, dayOff.getGroup_request_id() + "");

        return calendar;
    }


    @SuppressLint("SimpleDateFormat")
    public void requestDayOff(RunUi runUi) {
        boolean isValid = true;

        if (reason.get().trim().equals("")) {
            isValid = false;
            addError("reason", msg_invalid, true);
        } else {
            clearErro("reason");
        }

        if (isValid) {
            isLoading.set(true);
            //xử lý chuyển từ ngày đến ngày thành danh sách ngày nghỉ.
            List<DayOff> selectedDate = new ArrayList<>();
            startDate.get().setReason(reason.get());
            selectedDate.add(startDate.get());

            if (endDate.get().getCalendar() != null) {
                java.util.Calendar start = java.util.Calendar.getInstance();
                start.set(java.util.Calendar.YEAR, startDate.get().getCalendar().getYear());
                start.set(java.util.Calendar.MONTH, startDate.get().getCalendar().getMonth() - 1);
                start.set(java.util.Calendar.DATE, startDate.get().getCalendar().getDay());
                start.add(java.util.Calendar.DATE, 1);

                java.util.Calendar end = java.util.Calendar.getInstance();
                end.set(java.util.Calendar.YEAR, endDate.get().getCalendar().getYear());
                end.set(java.util.Calendar.MONTH, endDate.get().getCalendar().getMonth() - 1);
                end.set(java.util.Calendar.DATE, endDate.get().getCalendar().getDay());

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                //for loop từ ngày bắt đầu đến ngày kết thúc để thêm vào selectedDate
                for (Date date = start.getTime(); start.before(end); start.add(java.util.Calendar.DATE, 1), date = start.getTime()) {
                    DayOff dayOff = new DayOff(simpleDateFormat.format(date), DayOffType.WHOLE_DAY);
                    DayOff exits = mapDayOff.get(dayOff.getCalendar().toString());
                    if (exits != null && exits.getStatus().equals(DayOffStatus.CANCELED)) {
                        runUi.run("existDayOff");
                        isLoading.set(false);
                        return;
                    } else {
                        dayOff.setReason(reason.get());
                        selectedDate.add(dayOff);
                    }

                }
                endDate.get().setReason(reason.get());

                selectedDate.add(endDate.get());
            }

            DayOffApi.requestDayOff(selectedDate, reason.get(), new IResponse() {
                @Override
                public void onSuccess(Object o) {
                    isLoading.set(false);
                    String rs = (String) o;
                    long group_id = 0;
                    try {
                        group_id = Long.parseLong(rs);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (group_id > 0) {
                        //set lại view
                        for (DayOff dayOff : selectedDate) {
                            dayOff.setGroup_request_id(group_id);
                            dayOff.setStatus(DayOffStatus.WAITING);//set lại trạng thái của những ngày vừa xin nghỉ
                        }
                        dumpData(selectedDate, false);
                        runUi.run("requestSuccess");
                    } else {
                        runUi.run("requestFail");
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    runUi.run("requestFail");
                    isLoading.set(false);
                }
            });
        }
    }

    public void cancelDayOff(RunUi runUi) {
        isLoading.set(true);
        long group_id;
        if(getModelE().getOtherPart() != null){
            if((getModelE().getType().equals(DayOffType.MORNING) && isViewMorning.get())
                    || (getModelE().getType().equals(DayOffType.MORNING) && isViewMorning.get())){
                group_id = getModelE().getGroup_request_id();
            }else {
                group_id = getModelE().getOtherPart().getGroup_request_id();
            }
        }else {
            group_id = getModelE().getGroup_request_id();
        }

        DayOffApi.cancelDayOff(group_id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                OdooResultDto<DayOff> rs = (OdooResultDto<DayOff>) o;
                if (TsUtils.isNotNull(rs.getRecords())) {

                    for (DayOff changed : rs.getRecords()) {

                        DayOff dayOff = mapDayOff.get(changed.getDateFormatted());
                        assert dayOff != null;
                        if (dayOff.getGroup_request_id().equals(changed.getGroup_request_id())) {//kiểm tra 1 ngày có 2 lịch nghỉ thì đang hủy lịch nghỉ nào.
                            dayOff.setStatus(DayOffStatus.CANCELED);//set lại trạng thái là tự hủy.
                            Calendar calendar = getSchemeCalendar(dayOff);
                            dayOff.setCalendar(calendar);
                            mScheme.put(changed.getDateFormatted(), calendar);
                        } else {
                            if (dayOff.getOtherPart() != null) {
                                dayOff.getOtherPart().setStatus(DayOffStatus.CANCELED);//set lại trạng thái là tự hủy.
                                dayOff.getOtherPart().setCalendar(getSchemeCalendar(dayOff.getOtherPart()));

                                Calendar calendar = getSchemeCalendar(dayOff);
                                dayOff.setCalendar(calendar);
                                mScheme.put(changed.getDateFormatted(), calendar);
                            }
                        }
                    }


                    runUi.run("cancelSuccess");
                } else {
                    runUi.run("cancelFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("cancelFail");
            }
        });
    }


    private int getColor(String status) {
        int color = color_normal;
        if (status.equals(DayOffStatus.ACCEPTED)) {
            color = color_accept;
        } else if (status.equals(DayOffStatus.WAITING)) {
            color = color_pending;
        } else if (status.equals(DayOffStatus.CANCELED) || status.equals(DayOffStatus.DENIED)) {
            color = color_denied_cancel;
        }
        return color;
    }

    /**
     * for loop entrySet của hashMap sẽ đi từ đỉnh map về. Nên list sẽ được sắp xếp từ lớn đến bé
     *
     * @param selected selected calendar
     * @return danh sách ngày nghỉ trùng group_id với selected
     */
    public List<Calendar> getGroupDayOff(Calendar selected) {
        List<Calendar> calendars = new ArrayList<>(0);

        if (!selected.hasScheme()) {
            getModel().set(mapDayOff.get(selected.toString()));
            calendars.add(selected);
            return calendars;
        }

        DayOff dayOff = mapDayOff.get(selected.toString());//nếu chạy dc đến đây thì dayOff != null

        for (Map.Entry me : mapDayOff.entrySet()) {
            DayOff calendar = (DayOff) me.getValue();
            if (dayOff.getOtherPart() != null && calendar.getOtherPart() != null) {
                String dScheme = dayOff.getCalendar().getSchemes().get(2).getScheme();
                String dOtherScheme = dayOff.getOtherPart().getCalendar().getSchemes().get(2).getScheme();
                String cScheme = calendar.getCalendar().getSchemes().get(2).getScheme();
                String cOtherScheme = calendar.getOtherPart().getCalendar().getSchemes().get(2).getScheme();

                if (dScheme.equals(cScheme) || dOtherScheme.equals(cScheme)
                        || dScheme.equals(cOtherScheme) || dOtherScheme.equals(cOtherScheme)) {
                    calendars.add(calendar.getCalendar());
                }
            } else if (dayOff.getOtherPart() != null) {
                String dScheme = dayOff.getCalendar().getSchemes().get(2).getScheme();
                String dOtherScheme = dayOff.getOtherPart().getCalendar().getSchemes().get(2).getScheme();
                String cScheme = calendar.getCalendar().getSchemes().get(2).getScheme();

                if (dScheme.equals(cScheme) || dOtherScheme.equals(cScheme)) {
                    calendars.add(calendar.getCalendar());
                }
            } else if (calendar.getOtherPart() != null) {
                String dScheme = dayOff.getCalendar().getSchemes().get(2).getScheme();
                String cScheme = calendar.getCalendar().getSchemes().get(2).getScheme();
                String cOtherScheme = calendar.getOtherPart().getCalendar().getSchemes().get(2).getScheme();

                if (dScheme.equals(cScheme) || dScheme.equals(cOtherScheme)) {
                    calendars.add(calendar.getCalendar());
                }
            } else {
                String dScheme = dayOff.getCalendar().getSchemes().get(2).getScheme();
                String cScheme = calendar.getCalendar().getSchemes().get(2).getScheme();
                if (dScheme.equals(cScheme)) {
                    calendars.add(calendar.getCalendar());
                }
            }

        }

        //lấy ra start và end
        Calendar start = calendars.get(0);
        Calendar end = calendars.get(0);
        for (Calendar calendar : calendars) {
            if (calendar.differ(start) <= 0) {
                start = calendar;
            }
            if (calendar.differ(end) >= 0) {
                end = calendar;
            }
        }


        getModel().set(dayOff);
        // TODO: 28/11/2020 còn lỗi set _1stStart, _1stEnd
        if (dayOff.getOtherPart() != null) {
            if (dayOff.getType().equals(DayOffType.MORNING)) {
                _1stPartEnd.set(dayOff);
                _2ndPartStart.set(dayOff.getOtherPart());
            } else {
                _1stPartEnd.set(dayOff.getOtherPart());
                _2ndPartStart.set(dayOff);
            }

            _1stPartStart.set(mapDayOff.get(start.toString()));
            _2ndPartEnd.set(mapDayOff.get(end.toString()));
        } else {
            isViewMorning.set(true);
            _1stPartStart.set(mapDayOff.get(start.toString()));
            _1stPartEnd.set(mapDayOff.get(end.toString()));

            _2ndPartStart.set(null);
            _2ndPartEnd.set(null);
        }

        _1stPartStart.notifyChange();
        _1stPartEnd.notifyChange();
        _2ndPartStart.notifyChange();
        _2ndPartEnd.notifyChange();
        model.notifyChange();

        calendars.clear();
        calendars.add(start);
        calendars.add(end);
        return calendars;
    }
}
