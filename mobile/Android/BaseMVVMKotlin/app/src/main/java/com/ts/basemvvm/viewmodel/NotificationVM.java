package com.ts.basemvvm.viewmodel;

import android.app.Application;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.basemvvm.api.NotificationAPI;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.model.NotificationModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationVM extends BaseViewModel {
    ObservableBoolean isLoading = new ObservableBoolean();
    private int currentPage = 0;
    private int totalPage = 0;


    private ObservableInt records = new ObservableInt();
    private List<NotificationModel> listNotification;

    public NotificationVM(@NonNull Application application) {
        super(application);
        listNotification = new ArrayList<>();
    }


    public void getNotification(Integer currentPage, String type,  RunUi runUi){
        isLoading.set(true);
        NotificationAPI.getNotifications(type, currentPage, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                OdooResultDto<NotificationModel> result = (OdooResultDto<NotificationModel>) o;
                records.set(result.getTotal_record());
                listNotification.addAll(result.getRecords());
                if (result.getTotal_record() % 10 == 0) {
                    totalPage = result.getTotal_record() / 10;
                } else {
                    totalPage = result.getTotal_record() / 10 + 1;
                }
                runUi.run("getNotificationSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getNotificationFail");
            }
        });
    }

    public void getMoreNotifications(String type, RunUi runUi) {
        currentPage += 1;
        if (totalPage > 0 && currentPage < totalPage) {
            getNotification(currentPage, type,  runUi);
        } else {
            runUi.run("noMore");
        }
    }

    public void onRefresh(String type, RunUi runUi) {
        currentPage = 0;
        totalPage = 0;
        listNotification.clear();
        getNotification(currentPage, type,  runUi);
    }

    public void seenNotification(Long id){
        NotificationAPI.seenNotification(id, new IResponse() {
            @Override
            public void onSuccess(Object o) {

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
