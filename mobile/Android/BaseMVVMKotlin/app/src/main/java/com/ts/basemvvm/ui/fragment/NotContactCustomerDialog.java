package com.ts.basemvvm.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.ImageBaseAdapter;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.databinding.NotContactCustomerDialogBinding;
import com.ts.basemvvm.utils.FileUltil;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.NotContactCustomerDialogVM;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NotContactCustomerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    View.OnClickListener onClickListener;
    public NotContactCustomerDialogVM notContactCustomerDialogVM;
    private final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG = 9999;
    private final int REQUEST_CAMERA = 1100;

    NotContactCustomerDialogBinding mBinding;
    Calendar calendar;
    boolean isChooseDate = false;
    CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
    ImageBaseAdapter imageBaseAdapter;

    public NotContactCustomerDialog(View.OnClickListener onClickListener, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.onClickListener = onClickListener;
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.not_contact_customer_dialog, container, false);
        notContactCustomerDialogVM = ViewModelProviders.of(this).get(NotContactCustomerDialogVM.class);
        mBinding.setViewModel(notContactCustomerDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        setUpRecycleView();
        return mBinding.getRoot();
    }


    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG);
            }
        } else {
            openCamera();
        }
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void selectTime() {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), (view, hour1, minute1) -> {
                calendar.set(Calendar.HOUR_OF_DAY, hour1);
                calendar.set(Calendar.MINUTE, minute1);
                mBinding.txtTime.setText(AppController.formatTime.format(calendar.getTime()));
            }, hour, minute, DateFormat.is24HourFormat(getActivity()));
            timePickerDialog.show();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Uri uri = getImageUri(getContext(), bitmap);
            notContactCustomerDialogVM.getIsNotEmptyImage().set(true);
            notContactCustomerDialogVM.getListImage().add(uri.toString());
            notContactCustomerDialogVM.getListUrl().add(FileUltil.getPath(uri, getContext()));
            imageBaseAdapter.notifyItemInsert(notContactCustomerDialogVM.getListImage().size() - 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG) {
            for (int grant : grantResults) {
                if (grant != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            openCamera();
        }
    }

    public void selectDate() {
        Context scene = getContext();
        if (scene != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            Integer year = c.get(Calendar.YEAR);
            Integer month = c.get(Calendar.MONTH);
            Integer day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
            datePickerDialog.show();
        }
    }

    private void setUpRecycleView() {
        imageBaseAdapter = new ImageBaseAdapter(getActivity(), R.layout.item_image_rotate, notContactCustomerDialogVM.getListImage());
        mBinding.rcImages.setAdapter(imageBaseAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        mBinding.rcImages.setLayoutManager(layoutManager);
    }


    private void initView() {
        mBinding.btnCancel.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnDismiss.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnConfirm.setOnClickListener(onClickListener);
        mBinding.cbChooseDate.setOnCheckedChangeListener(onCheckedChangeListener);

        mBinding.btnAddImage.setOnClickListener(v -> selectImages());
        mBinding.txtDate.setOnClickListener(v -> selectDate());
        calendar = Calendar.getInstance();
    }

    public List<String> getListImage() {
        if (notContactCustomerDialogVM.getListUrl() != null && notContactCustomerDialogVM.getListUrl().size() > 0) {
            return notContactCustomerDialogVM.getListUrl();
        }
        ToastUtils.showToast(getActivity(), getString(R.string.NOT_IMAGE), R.drawable.ic_close14);
        return null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(year, month, dayOfMonth);
        isChooseDate = true;
        mBinding.txtDate.setText(AppController.formatDate.format(calendar.getTime()));
    }

    public String getDate() {
        return AppController.formatTimeOdooDateTime.format(calendar.getTime());
    }
}
