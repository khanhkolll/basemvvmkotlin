package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.XBaseAdapter;
import com.ts.basemvvm.databinding.VehicleHistoryFragmentBinding;
import com.ts.basemvvm.utils.ToastUtils;
import com.ts.basemvvm.viewmodel.VehicleHistoryVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class VehicleHistoryFragment extends BaseFragment implements CalendarView.OnCalendarSelectListener,
        CalendarView.OnYearChangeListener {
    private VehicleHistoryFragmentBinding mBinding;
    private VehicleHistoryVM vehicleHistoryVM;
    private int mYear;

    private XBaseAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (VehicleHistoryFragmentBinding) binding;
        vehicleHistoryVM = (VehicleHistoryVM) viewModel;
        initToolBar();
        initView();
        return v;
    }

    @SuppressLint("SetTextI18n")
    private void initToolBar() {
        mBinding.btnBack.setOnClickListener(v -> {
            if (mBinding.calendar.isYearSelectLayoutVisible()) {
                mBinding.calendar.closeYearSelectLayout();
            } else {
                getActivity().onBackPressed();
            }
        });
        mBinding.tvMonthDay.setOnClickListener(v -> {
            if (!mBinding.calendarLayout.isExpand()) {
                mBinding.calendarLayout.expand();
                return;
            }
            mBinding.calendar.showYearSelectLayout(mYear);
            mBinding.tvYear.setVisibility(View.GONE);
            mBinding.tvMonthDay.setText(String.valueOf(mYear));
        });
        mBinding.flCurrent.setOnClickListener(v -> mBinding.calendar.scrollToCurrent());
        mBinding.tvYear.setText(String.valueOf(mBinding.calendar.getCurYear()));
        mYear = mBinding.calendar.getCurYear();
        String monthStr = mBinding.calendar.getCurMonth() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        String dayStr = mBinding.calendar.getCurDay() + "";
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvCurrentDay.setText(String.valueOf(mBinding.calendar.getCurDay()));
    }

    private void initView() {
        //init calendar view
        mBinding.calendar.setRange(2020, 6, 1,
                mBinding.calendar.getCurYear(), mBinding.calendar.getCurMonth(), mBinding.calendar.getCurDay());
        mBinding.calendar.setOnCalendarSelectListener(this);
        mBinding.calendar.setOnYearChangeListener(this);
        mBinding.calendar.setSelectDefaultMode();
        //lấy thông tin ngày hiện tại
        vehicleHistoryVM.getVehicleHistory(mBinding.calendar.getCurYear() + "-"
                        + mBinding.calendar.getCurMonth() + "-"
                        + mBinding.calendar.getCurDay()
                , this::runUi);

        //init recycle view
        adapter = new XBaseAdapter(R.layout.item_vehicle_history, vehicleHistoryVM.getVehicleHistory(), this);
        mBinding.rcVanByDay.setAdapter(adapter);

    }


    public void scrollToCurrent() {
        mBinding.calendar.scrollToCurrent();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.vehicle_history_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return VehicleHistoryVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {
        ToastUtils.showToast(getString(R.string.can_not_get_vehicle_history));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        String dayStr = calendar.getDay() + "";
        String monthStr = calendar.getMonth() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvYear.setVisibility(View.VISIBLE);
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvYear.setText(String.valueOf(calendar.getYear()));
        mYear = calendar.getYear();

        if (isClick) {
            vehicleHistoryVM.getVehicleHistory(calendar.getYear() + "-" + calendar.getMonth() + "-" + calendar.getDay(), this::runUi);
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getHistorySuccess":
                adapter.notifyDataSetChanged();
                break;
            case "getHistoryFail":
                ToastUtils.showToast(getContext(), R.string.some_thing_wrong);
                break;
        }
    }

    @Override
    public void onYearChange(int year) {
        mBinding.tvMonthDay.setText(String.valueOf(year));
    }
}
