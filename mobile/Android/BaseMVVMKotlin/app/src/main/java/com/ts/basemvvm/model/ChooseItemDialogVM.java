package com.ts.basemvvm.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChooseItemDialogVM extends BaseViewModel {
    ObservableBoolean isReload= new ObservableBoolean();
    public ChooseItemDialogVM(@NonNull Application application) {
        super(application);
        isReload.set(true);
    }
}
