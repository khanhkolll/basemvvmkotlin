package com.ts.basemvvm.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.king.zxing.util.CodeUtils;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.enums.RoutingPlanDayStatus;
import com.ts.basemvvm.enums.RoutingType;
import com.ts.basemvvm.enums.TroubleType;
import com.ts.basemvvm.model.BiddingInformation;
import com.ts.basemvvm.model.BillPackage;
import com.ts.basemvvm.model.CargoType;
import com.ts.basemvvm.model.Reward;
import com.ts.basemvvm.model.ShareVanRoutingPlan;
import com.ts.basemvvm.model.section.ItemRoutingNote;
import com.ts.basemvvm.model.section.RootNode;

import java.util.Calendar;
import java.util.List;

public class BindingAdapterUtil {

    @BindingAdapter("textChanged")
    public static void quantityPackageChange(EditText et, Integer number) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!StringUtils.isNumberInt(et.getText().toString())) {
                    ((TextInputLayout) et.getParent().getParent()).setError(et.getContext().getResources().getString(R.string.error_quantity));
                    return;
                }
                if (Double.parseDouble(et.getText().toString()) > number) {
                    ((TextInputLayout) et.getParent().getParent()).setError(et.getContext().getResources().getString(R.string.error_quantity));
                } else {
                    ((TextInputLayout) et.getParent().getParent()).setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @BindingAdapter("textChanged")
    public static void quantityReturnEquipment(EditText et, Integer number) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!StringUtils.isNumberInt(et.getText().toString())) {
                    et.setError(et.getContext().getResources().getString(R.string.error_equipment_quantity));
                    return;
                }
                if (Double.parseDouble(et.getText().toString()) > number) {
                    et.setError(et.getContext().getResources().getString(R.string.error_equipment_quantity));
                } else {
                    et.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @BindingAdapter("orderType")
    public static void orderType(TextView view, String type) {
        if (type.equals(RoutingType.Pickup)) {
            view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_up, 0, 0, 0);
            view.setText(R.string.pickup);
        } else {
            view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_down, 0, 0, 0);
            view.setText(R.string.drop);

        }
    }
    @BindingAdapter("status_routing")
    public static void status_routing(TextView view, Integer statusRouting) {
        Resources resources= view.getResources();
        switch (statusRouting){
            case 1:
                view.setText(resources.getString(R.string.pending));
                break;
            case 2:
                view.setText(resources.getString(R.string.driver_confirm));
                break;
            case 3:
                view.setText(resources.getString(R.string.Complete));
                break;
            case 4:
                view.setText(resources.getString(R.string.CANCELLED));
                break;
            case 5:
                view.setText(resources.getString(R.string.PENDING));
                break;
            default:
                view.setText(resources.getString(R.string.ALL));
                break;
        }
    }

    @BindingAdapter("orderStatus")
    public static void orderStatus(TextView view, Integer status) {
        Resources res = view.getResources();
        if (status == null) return;
        switch (status) {
            case 0:
                view.setTextColor(res.getColor(R.color.color_normal_text));
                view.setText(res.getString(R.string.pending));
                break;
            case 1:
                view.setTextColor(res.getColor(R.color.color_pending));
                view.setText(res.getString(R.string.driver_confirm));
                break;
            case 2:
                view.setTextColor(res.getColor(R.color.primaryColor));
                view.setText(res.getString(R.string.SUCCESS));
                break;
            case 3:
                view.setTextColor(res.getColor(R.color.color_price));
                view.setText(res.getString(R.string.CANCELLED));
                break;
            case 4:
                view.setTextColor(res.getColor(R.color.color_pending));
                view.setText(res.getString(R.string.Processing));
                break;
        }
    }

    @BindingAdapter("orderHistoryStatus")
    public static void orderHistoryStatus(TextView view, Integer status) {
        Resources res = view.getResources();
        if (status == null) return;
        switch (status) {
            case 0:
                view.setTextColor(res.getColor(R.color.color_normal_text));
                view.setText(res.getString(R.string.pending));
                break;
            case 1:
                view.setTextColor(res.getColor(R.color.color_pending));
                view.setText(res.getString(R.string.driver_confirm));
                break;
            case 2:
                view.setTextColor(res.getColor(R.color.primaryColor));
                view.setText(res.getString(R.string.SUCCESS));
                break;
            case 3:
                view.setTextColor(res.getColor(R.color.color_price));
                view.setText(res.getString(R.string.CANCELLED));
                break;
            case 4:
                view.setTextColor(res.getColor(R.color.color_pending));
                view.setText(res.getString(R.string.Processing));
                break;
        }
    }

    @BindingAdapter("orderStatus")
    public static void orderStatus(ImageView view, Integer status) {
        Resources res = view.getResources();
        if (status == null) return;
        switch (status) {
            case 0:
                view.setImageDrawable(res.getDrawable(R.drawable.ic_vans));
                break;
            case 1:
                view.setImageDrawable(res.getDrawable(R.drawable.ic_vans_green));
                break;
            case 2:
                view.setImageDrawable(res.getDrawable(R.drawable.ic_radio_green));
                break;
            case 3:
                view.setImageDrawable(res.getDrawable(R.drawable.ic_radio_red));
                break;
            case 4:
                view.setImageDrawable(res.getDrawable(R.drawable.ic_radio_pending));

                break;
        }
    }

    @BindingAdapter("orderStatus")
    public static void orderStatus(ViewGroup view, Integer status) {
        Resources res = view.getResources();
        switch (status) {
            case 1:
                view.setBackground(res.getDrawable(R.drawable.bg_border_green_left));
                break;
            case 0:
            case 2:
            case 4:
                view.setBackground(res.getDrawable(R.drawable.bg_border_black_alpha_left));
                break;
        }
    }

    @BindingAdapter("hideBtnRatingCustomer")
    public static void hideBtnRatingCustomer(ConstraintLayout view, ShareVanRoutingPlan routingPlan) {
        if (routingPlan != null) {
            if (routingPlan.getStatus() != null && routingPlan.getStatus().equals(RoutingPlanDayStatus.COMPLETED) && routingPlan.getAccept_time() != null) {
                if(TroubleType.RETURN.equals(routingPlan.getTrouble_type())){
                    view.setVisibility(View.GONE);
                }
                else if (routingPlan.isRating_customer()) {
                    view.setVisibility(View.VISIBLE);
                }
                else if ((Calendar.getInstance().getTime().getTime() - routingPlan.getAccept_time().getTime()) / (1000 * 60 * 60) < StaticData.getOdooSessionDto().getRating_customer_duration_key()) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
            } else {
                view.setVisibility(View.GONE);
            }
            return;
        }
    }

    @BindingAdapter({"routingPlan", "isRequire"})
    public static void showButtonCancelRouting(LinearLayout view, ShareVanRoutingPlan routingPlan, Boolean isRequire) {
        if (routingPlan != null && routingPlan.getStatus() != null && routingPlan.getType() != null) {
            if (RoutingType.Drop.equals(routingPlan.getType()) || !isRequire || routingPlan.getStatus() != 0 || !routingPlan.isArrived_check() || routingPlan.getSo_type()) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    @BindingAdapter("icon_routing_detail")
    public static void icon_routing_detail(TextView view, ItemRoutingNote itemRoutingNote) {
        if (itemRoutingNote != null) {
            Resources resources = view.getResources();
            switch (itemRoutingNote.getStatus()) {
                case 1:
                case 4:
                    if (itemRoutingNote.checked) {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_warning_white), null);
                    } else {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_warning), null);
                    }
                    break;
                case 2:
                    if (itemRoutingNote.checked) {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_check_white), null);
                    } else {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_check), null);
                    }
                    break;
                case 3:
                    if (itemRoutingNote.checked) {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_warning_white), null);
                    } else {
                        view.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_warning_red), null);
                    }
                    break;
                default:
                    break;
            }
        }

    }

    @BindingAdapter({"shareVanRoutingPlan", "quantity"})
    public static void showQrChecked(ImageView view, String status, Integer type) {
        switch (type) {
            case 0:
                if (status.equals("0")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
            case 1:
                if (!status.equals("2")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
        }
    }

    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, OdooDateTime time) {
        if (time != null) {
            if ((System.currentTimeMillis() - time.getTime()) < 60 * 1000) {
                view.setText(R.string.now);
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        time.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.ic_stub));
            return;
        }
        loadImage(view, imageUrl, view.getContext(),null);
    }
    @BindingAdapter("imageUrl_rotate")
    public static void loadImageRotate(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.ic_stub));
            return;
        }
        loadImage(view, imageUrl, view.getContext(),true);
    }

    @BindingAdapter("routing_number_order")
    public static void routing_number_order(TextView view, RootNode rootNode) {
        if (rootNode != null) {
            view.setText(rootNode.getWarehouseName() + " ( " + rootNode.getChildNode().size() + " )");
        }
    }

    @BindingAdapter({"isArrived", "status_routing"})
    public static void show_notify_routing_day(LinearLayout view, Boolean isRequire, ShareVanRoutingPlan routingPlan) {
        if (isRequire != null && routingPlan != null && routingPlan.getTrouble_type() != null) {
            if (isRequire) {
                if (!RoutingPlanDayStatus.SHIPPING.equals(routingPlan.getStatus()) || (routingPlan.isArrived_check() && !RoutingType.Pickup.equals(routingPlan.getType()) && !TroubleType.NORMAL.equals(routingPlan.getTrouble_type()))) {
                    view.setVisibility(View.GONE);
                } else if (!RoutingType.Pickup.equals(routingPlan.getType())) {
                    view.setVisibility(View.VISIBLE);
                }
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("fromWarehouse")
    public static void setWarehouseName(TextView view, List<BillPackage> list_bill_package) {
        if (list_bill_package == null || list_bill_package.size() <= 0) {
            return;
        }
        switch (view.getId()) {
            case R.id.txtPickupAddress:
                view.setText(list_bill_package.get(0).getAddress());
                break;
            case R.id.txtPickupWarehouseName:
                view.setText(list_bill_package.get(0).getWarehouse_name());
                break;
            case R.id.txtPickupPhone:
                view.setText(list_bill_package.get(0).getPhone());
                break;
            case R.id.txtPickupRoutingCode:
                view.setText(list_bill_package.get(0).getRouting_plan_day_code());
                break;
        }
    }

    @BindingAdapter("setWarehouseReturn")
    public static void setWarehouseReturn(TextView view, ShareVanRoutingPlan routingPlan) {
        if (routingPlan != null) {
            switch (view.getId()) {
                case R.id.txtWarehouseReturnAddress:
                    view.setText(routingPlan.getAddress());
                    break;
                case R.id.txtWarehouseReturnName:
                    view.setText(routingPlan.getWarehouse_name());
                    break;
                case R.id.txtWarehouseReturnPhone:
                    view.setText(routingPlan.getPhone());
                    break;
                case R.id.txtWarehouseReturnRoutingCode:
                    view.setText(routingPlan.getRouting_plan_day_code());
                    break;
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @BindingAdapter("rewardInfo")
    public static void rewardInfo(ConstraintLayout view, List<Reward> title_award) {
        if (title_award == null || title_award.size() <= 0) {
            return;
        }
        ImageView imgReward = view.findViewById(R.id.imgReward);
        TextView txtReward = view.findViewById(R.id.txtReward);
        TextView txtPoint = view.findViewById(R.id.txtPoint);

        txtPoint.setText(StaticData.getDriver().getPoint() + " " + view.getContext().getResources().getString(R.string.point));
        loadImage(imgReward, title_award.get(title_award.size() - 1).getIcon(), view.getContext(),null);
        txtReward.setText(title_award.get(title_award.size() - 1).getName());
        for (Reward reward : title_award) {
            if (StaticData.getDriver().getPoint() >= reward.getFrom_point()
                    && StaticData.getDriver().getPoint() <= reward.getTo_point()) {
                loadImage(imgReward, reward.getIcon(), view.getContext(),null);
                txtReward.setText(reward.getName());
                break;
            }
        }

    }
    @SuppressLint("SetTextI18n")
    @BindingAdapter("rewardInfoD")
    public static void rewardInfoD(TextView view, List<Reward> title_award) {
        if (title_award == null || title_award.size() <= 0) {
            return;
        }
        view.setText(title_award.get(title_award.size() - 1).getName());
        for (Reward reward : title_award) {
            if (StaticData.getDriver().getPoint() >= reward.getFrom_point()
                    && StaticData.getDriver().getPoint() <= reward.getTo_point()) {
                view.setText(reward.getName());
                break;
            }
        }
    }

        //    AppController.BASE_URL_IMAGE +
    private static void loadImage(ImageView imgReward, String icon, Context context,Boolean isRotate) {
//        GlideUrl url = new GlideUrl(icon, new LazyHeaders.Builder()
//                .addHeader("Cookie", StaticData.sessionCookie)
//                .build());
        Glide.with(context)
                .load(icon)
                .dontAnimate()
                .placeholder(R.drawable.ic_stub)
                .error(R.drawable.ic_stub)
                .into(imgReward);
        if (isRotate != null) {
            if(imgReward.getHeight()<imgReward.getWidth()&&isRotate){
                imgReward.setRotation(90);
            }
        }
    }


    @BindingAdapter("loadImageCancelOrder")
    public static void loadImageCancelOrder(ImageView image, Uri uri) {
        if (uri != null) {
            image.setImageURI(uri);
        }
    }

    @BindingAdapter("showDate")
    public static void showDate(TextView textView, OdooDate date) {
        if (date != null) {
            textView.setText(AppController.formatDate.format(date));
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @BindingAdapter("textHtml")
    public static void textHtml(WebView webview, String description) {
        if (description != null) {
            final WebSettings webSettings = webview.getSettings();
            webSettings.setDefaultFontSize(26);
            webSettings.setDefaultTextEncodingName("utf-8");
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            webSettings.setAppCacheEnabled(false);
            webSettings.setBlockNetworkImage(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setGeolocationEnabled(false);
            webSettings.setNeedInitialFocus(false);
            webSettings.setSaveFormData(false);

            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);

            webview.loadData(description, "text/html", "UTF-8");
        }

    }

    @SuppressLint("SetTextI18n")
    @BindingAdapter({"biddingOrderFromTime", "biddingOrderToTime"})
    public static void biddingOrderTime(TextView view, OdooDateTime fromTime, OdooDateTime toTime) {
        if (fromTime == null && toTime != null)
            view.setText(AppController.formatDateTime.format(toTime));
        if (fromTime != null && toTime == null)
            view.setText(AppController.formatDateTime.format(fromTime));
        if (fromTime != null && toTime != null)
            view.setText(AppController.formatDateTime.format(fromTime) + "  ->  " + AppController.formatDateTime.format(toTime));
        if (fromTime == null && toTime == null)
            view.setText("");
    }


    @BindingAdapter("setListIdCargo")
    public static void setListIdCargo(LinearLayout linearLayout, BiddingInformation biddingShipping) {
        linearLayout.removeAllViews();
        try {
            if (biddingShipping.getBidding_vehicles().getBidding_order_receive().getStatus().equals("0")) {
                // case bidding chưa vận chuyển sẽ k có thông tin quality và type của bidding
                linearLayout.setVisibility(View.GONE);
            } else {
                linearLayout.setVisibility(View.VISIBLE);
                // set số lượng quality và type của bidding
                for (int j = 0; j < biddingShipping.getBidding_vehicles().getCargo_types().size(); j++) {
                    CargoType cargoType = biddingShipping.getBidding_vehicles().getCargo_types().get(j);
                    addItemChill(linearLayout, cargoType);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static void addItemChill(LinearLayout linearLayout, CargoType cargoTypes) {
        String value;
        LayoutInflater mInflater = LayoutInflater.from(linearLayout.getContext());
        View newsListRow = mInflater.inflate(R.layout.item_total_cargo_info, null, false);
        TextView textView = newsListRow.getRootView().findViewById(R.id.tvCargo);

        String quality = cargoTypes.getCargo_quantity() + " cargos";
        String type = cargoTypes.getType();
        value = quality + " " + type;
        textView.setText(value);

        linearLayout.addView(newsListRow);
    }

    /**
     * @param textView view
     * @param status   0: chưa nhận, 1: đã nhận, 2:thành công
     */
    @BindingAdapter("biddingOrderStatus")
    public static void biddingOrderStatus(TextView textView, int status) {
        Resources res = textView.getResources();
        switch (status) {
            case 0:
                textView.setText(res.getString(R.string.not_yet_delivery));
                textView.setTextColor(res.getColor(R.color.color_normal_text));
                break;
            case 1:
                textView.setText(res.getString(R.string.in_transit));
                textView.setTextColor(res.getColor(R.color.color_pending));
                break;
            case 2:
                textView.setText(res.getString(R.string.success));
                textView.setTextColor(res.getColor(R.color.primaryColor));
                break;
        }
    }

    @BindingAdapter("genQR")
    public static void genQR(ImageView img, String qr) {
        if (qr == null) return;
//            Bitmap logo = BitmapFactory.decodeResource(img.getResources(), R.drawable.banner_app_new);
        Bitmap bitmap = CodeUtils.createQRCode(qr, 600);
        img.setImageBitmap(bitmap);
    }


}
