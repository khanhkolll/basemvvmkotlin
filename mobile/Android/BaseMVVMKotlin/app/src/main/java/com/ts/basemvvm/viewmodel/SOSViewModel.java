package com.ts.basemvvm.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.google.android.gms.maps.model.LatLng;
import com.luck.picture.lib.entity.LocalMedia;
import com.ts.basemvvm.api.SOSApi;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.base.RunUi;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.enums.SosStatus;
import com.ts.basemvvm.enums.VehicleStateStatus;
import com.ts.basemvvm.model.SOSType;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SOSViewModel extends BaseViewModel {
    public ObservableBoolean isNotEmptyImage = new ObservableBoolean();
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<SOSType> typeList;

    private List<LocalMedia> lstFileSelected;

    public SOSViewModel(@NonNull Application application) {
        super(application);
        typeList = new ArrayList<>();
    }

    public void getTypeList(RunUi runUi){
        isLoading.set(true);
        SOSApi.getSOS(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if(o != null){
                    typeList.addAll((List<SOSType>) o);
                    runUi.run("getSOSSuccess");
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);

            }
        });
    }

    public void createSOS(int position, int routing_plan_id, LatLng latLng,
                          boolean continuable, int delayTime, String address, int orderNumber, RunUi runUi){
        isLoading.set(true);
        if(StaticData.getDriver().getVehicle() == null){
            runUi.run("not_on_shipping");
            isLoading.set(false);
            return;
        }else if(!StaticData.getDriver().getVehicle().getStatus().equals(VehicleStateStatus.Shipping)){
            runUi.run("not_on_shipping");
            isLoading.set(false);
            return;
        }
        SOSApi.createSOS(typeList.get(position), routing_plan_id, lstFileSelected,
                latLng, continuable, delayTime, address, orderNumber + "", new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                runUi.run("create_sos_success");
                StaticData.getDriver().getVehicle().setSos_status(continuable ? SosStatus.CONTINUABLE : SosStatus.CRASH);
                Log.e("CREATE_SOS", "success");
            }
            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("create_sos_fail");
                Log.e("CREATE_SOS", "fail");
            }
        });
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }
}
