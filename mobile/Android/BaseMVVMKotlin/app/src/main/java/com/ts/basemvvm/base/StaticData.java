package com.ts.basemvvm.base;


import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.ts.basemvvm.model.Driver;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class StaticData {

    private static OdooSessionDto odooSessionDto;
    private static Driver driver;
    public  static String sessionCookie;

    public static final String FRAGMENT = "FRAGMENT";
    public static String TOKEN_DCOM = "";

    public static Driver getDriver() {
        return driver;
    }

    public static void setDriver(Driver driver) {
        StaticData.driver = driver;
    }

    public static OdooSessionDto getOdooSessionDto() {
        return odooSessionDto;
    }

    public static void setOdooSessionDto(OdooSessionDto odooSessionDto) {
        StaticData.odooSessionDto = odooSessionDto;
    }

    public static String getUserLogin() {
        if(odooSessionDto == null){
            return "";
        }
        return odooSessionDto.getUsername();
    }

}
