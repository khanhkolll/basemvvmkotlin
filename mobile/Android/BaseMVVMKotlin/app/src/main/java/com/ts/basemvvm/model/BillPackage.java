package com.ts.basemvvm.model;

import com.ts.basemvvm.base.AppController;
import com.tsolution.base.BaseModel;
import com.ts.basemvvm.utils.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BillPackage extends BaseModel implements Cloneable {
    private Integer quantityQrChecked = 0; // Số lượng qr đã check
    private Integer quantityClone = 0;
    private boolean qrChecked = false; // kiểm tra hoàn thành check qr
    private Integer id;
    private Integer quantity_import;
    private Double length;
    private Double width;
    private Double height;
    private Double total_weight;
    private Double capacity;
    private String item_name;
    private Integer product_type_id;
    private String note;
    private String product_type_name;
    private Integer bill_package_id;

    private String qr_char;


    //nếu nằm trong kho xuất.
    private Integer quantity_export;
    private String warehouse_name;
    private String routing_plan_day_code;
    private String address;
    private String phone;
    private Float latitude;
    private Float longitude;


    //thuộc tính dùng cho lúc cập nhật trạng thái
    private boolean isEdited;


    public void setQuantity_import_str(String str) {
        if (str.length() == 0) {
            str = "0";
        }
        this.quantity_import = Integer.parseInt(str);
    }

    public String getQuantity_import_str() {
        return this.quantity_import != null ? quantity_import + "" : "0";
    }

    public void setQuantity_export_str(String str) {
        if (str.length() == 0) {
            str = "0";
        }
        this.quantity_export = Integer.parseInt(str);
    }

    public String getQuantity_export_str() {
        return this.quantity_export != null ? quantity_export + "" : "0";
    }


    public String getLengthStr() {
        return this.getLength() != null ? AppController.getInstance().formatNumber(length * 100) : "";
    }

    public void setLengthStr(String lengthStr) {
        if (!StringUtils.isNullOrEmpty(lengthStr)) {
            if (lengthStr.charAt(0) == '.') {
                lengthStr = lengthStr.replace(".", "0.");
            }
            this.setLength(Double.parseDouble(lengthStr) / 100);
        } else {
            this.setLength(0d);
        }
    }

    //đưa về cm
    public String getWidthStr() {
        return this.getWidth() != null ? AppController.getInstance().formatNumber(width * 100) : "";
    }

    //đưa về m
    public void setWidthStr(String widthStr) {
        if (!StringUtils.isNullOrEmpty(widthStr)) {
            if (widthStr.charAt(0) == '.') {
                widthStr = widthStr.replace(".", "0.");
            }
            this.setWidth(Double.parseDouble(widthStr) / 100);
        } else {
            this.setWidth(0d);
        }
    }

    //đưa về cm
    public String getHeightStr() {
        return this.getHeight() != null ? AppController.getInstance().formatNumber(height * 100) : "";
    }

    //đưa về m
    public void setHeightStr(String heightStr) {
        if (!StringUtils.isNullOrEmpty(heightStr)) {
            if (heightStr.charAt(0) == '.') {
                heightStr = heightStr.replace(".", "0.");
            }
            this.setHeight(Double.parseDouble(heightStr) / 100);
        } else {
            this.setHeight(0d);
        }
    }

    public String getWeightStr() {
        return this.total_weight != null ? AppController.getInstance().formatNumber(total_weight) : "";
    }

    public void setWeightStr(String weightStr) {
        if (!StringUtils.isNullOrEmpty(weightStr)) {
            if (weightStr.charAt(0) == '.') {
                weightStr = weightStr.replace(".", "0.");
            }
            this.total_weight = (Double.parseDouble(weightStr));
        } else {
            this.total_weight = (0d);
        }
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
            // This should never happen
        }
    }

}
