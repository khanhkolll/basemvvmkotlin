package com.ts.basemvvm.model;


@lombok.Getter
@lombok.Setter
public class Warehouse {
    private Integer id;
    private String warehouse_code;
    private String name;
    private String address;
    private String street;
    private String city_name;
    private String district;
    private String phone;

}

