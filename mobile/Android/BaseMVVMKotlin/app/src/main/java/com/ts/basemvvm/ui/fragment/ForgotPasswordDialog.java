package com.ts.basemvvm.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.ts.basemvvm.R;
import com.ts.basemvvm.databinding.ForgotPasswordDialogBinding;
import com.ts.basemvvm.utils.StringUtils;
import com.ts.basemvvm.viewmodel.ForgotPasswordVM;

public class ForgotPasswordDialog extends DialogFragment {
    ForgotPasswordDialogBinding mBinding;
    ForgotPasswordVM forgotPasswordVM;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.forgot_password_dialog, container, false);
        forgotPasswordVM = ViewModelProviders.of(this).get(ForgotPasswordVM.class);
        mBinding.setViewModel(forgotPasswordVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return mBinding.getRoot();
    }

    private void initView() {
        mBinding.btnCancel.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnDismiss.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnConfirm.setOnClickListener(v->setUpPassword());
        mBinding.txtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mBinding.ipPhoneNumber.setErrorEnabled(false);
            }
        });
    }
    private void setUpPassword(){
        if(isValidate()){
            Toast.makeText(getContext(),"FORGOT_PASSWORD",Toast.LENGTH_SHORT);
        }
    }
    private boolean isValidate(){
        if(StringUtils.isNullOrEmpty(mBinding.txtPhoneNumber.getText().toString())){
            mBinding.ipPhoneNumber.setError(getString(R.string.INVALID_FIELD));
            return false;
        }

        return true;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
