package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.BottomSheetInfoDriverBinding;
import com.ts.basemvvm.model.Area;
import com.ts.basemvvm.viewmodel.MarketPlaceVM;
import com.tsolution.base.listener.AdapterListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class BottomSheetInfoDriver extends BottomSheetDialogFragment {
    BottomSheetInfoDriverBinding binding;
    MarketPlaceVM marketPlaceVM;
    onConfirm onConfirm;
    SimpleDateFormat format;
    private Date toDate;
    private Date fromDate;
    ListDialogFragment listDialogFragment;

    @SuppressLint("SimpleDateFormat")
    public BottomSheetInfoDriver(MarketPlaceVM marketPlaceVM, onConfirm onConfirm) {
        this.marketPlaceVM = marketPlaceVM;
        this.onConfirm = onConfirm;
        format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_info_driver, container, false);
        binding.setViewModel(marketPlaceVM);
        binding.setListener(this);

        binding.btnDismiss.setOnClickListener(v -> dismiss());

        Calendar cal = Calendar.getInstance();
        fromDate = cal.getTime();
        binding.txtFromDate.setText(format.format(fromDate.getTime()));
        marketPlaceVM.getDriverMarketInfo().get().setFrom_date_assign(format.format(fromDate.getTime()));

        cal.add(Calendar.DATE, 1);
        toDate = cal.getTime();
        binding.txtToDate.setText(format.format(toDate.getTime()));
        marketPlaceVM.getDriverMarketInfo().get().setTo_date_assign(format.format(toDate.getTime()));

        binding.btnConfirm.setOnClickListener(v -> {
            if (marketPlaceVM.isValid(getContext())) {
                marketPlaceVM.getDriverMarketInfo().get().setDriver_id(StaticData.getDriver().getId());
                marketPlaceVM.registerInfo(this::runUi);
            }
        });

        binding.btnDisconnect.setOnClickListener(v -> {
            marketPlaceVM.getIsOnline().set(false);
            dismiss();
        });

        return binding.getRoot();

    }

    @SuppressLint("NonConstantResourceId")
    public void selectDate(View v) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);

                String date = format.format(calendar.getTime());

                switch (v.getId()) {
                    case R.id.txtFromDate:
                        ((TextView) v).setText(date);
                        fromDate.setTime(calendar.getTime().getTime());
                        marketPlaceVM.getDriverMarketInfo().get().setFrom_date_assign(date);
                        break;
                    case R.id.txtToDate:
                        ((TextView) v).setText(date);
                        toDate.setTime(calendar.getTime().getTime());
                        marketPlaceVM.getDriverMarketInfo().get().setTo_date_assign(date);
                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
            } else if (v.getId() == R.id.txtFromDate) {
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMaxDate(toDate.getTime());
            }

            datePickerDialog.show();
        }
    }

    public void selectArea(View view) {
        if (marketPlaceVM.getListArea().size() > 0) {
            initDialogArea((TextInputEditText) view);
        } else {
            marketPlaceVM.getAreas(view, this::runUi);
        }
    }

    private void initDialogArea(TextInputEditText v) {
        int title = v.getId() == R.id.txtToArea ? R.string.to_area : R.string.from_area;
        listDialogFragment = new ListDialogFragment(R.layout.item_area, title,
                marketPlaceVM.getListArea(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                Area area = (Area) o;
                listDialogFragment.dismiss();
                v.setText(area.getName());
                if (v.getId() == R.id.txtToArea) {
                    marketPlaceVM.getDriverMarketInfo().get().setTo_area(area);
                    binding.lbToArea.setError(null);
                } else if (v.getId() == R.id.txtFromArea) {
                    marketPlaceVM.getDriverMarketInfo().get().setFrom_area(area);
                    binding.lbFromArea.setError(null);
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listDialogFragment.show(getChildFragmentManager(), "area");
    }

    private void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getAreaSuccess":
                initDialogArea((TextInputEditText) objects[1]);
                break;
            case "getAreaFail":
                Toast.makeText(getContext(), R.string.msg_get_area_fail, Toast.LENGTH_SHORT).show();
                break;
            case "connectSuccess":
                dismiss();
                break;
            case "connectFail":
                Toast.makeText(getContext(), R.string.can_not_connect, Toast.LENGTH_SHORT).show();
                break;
            case "getDriverMarketInfoSuccess":
                try {
                    fromDate = format.parse(marketPlaceVM.getDriverMarketInfo().get().getFrom_date_assign());
                    toDate = format.parse(marketPlaceVM.getDriverMarketInfo().get().getTo_date_assign());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        onConfirm.onConfirm(marketPlaceVM.getIsOnline().get());
        super.onDismiss(dialog);
    }

    public interface onConfirm {
        void onConfirm(boolean status);
    }

}
