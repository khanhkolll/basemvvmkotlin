package com.ts.basemvvm.socket_notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ts.basemvvm.model.BiddingInformation;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SocketResult extends BaseModel {


    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;


    }

    @Setter
    @Getter
    public class Result extends BaseModel {

        @SerializedName("records")
        @Expose
        private List<BiddingInformation> records = null;
    }


}
