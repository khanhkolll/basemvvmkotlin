package com.ts.basemvvm.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.BaseAdapterV3;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.RoutingHistoryFragmentBinding;
import com.ts.basemvvm.model.ShareVanRoutingPlan;
import com.ts.basemvvm.viewmodel.RoutingHistoryVM;
import com.ts.basemvvm.widget.CustomLoadMoreView;
import com.ts.basemvvm.widget.OnSingleClickListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RoutingHistoryFragment extends BaseFragment {
    private BaseAdapterV3 adapter;
    private RoutingHistoryFragmentBinding mBinding;
    private RoutingHistoryVM routingHistoryVM;
    private Date toDate;
    private Date fromDate;
    SimpleDateFormat format;

    @SuppressLint("SimpleDateFormat")
    public RoutingHistoryFragment() {
        format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (RoutingHistoryFragmentBinding) binding;
        routingHistoryVM = (RoutingHistoryVM) viewModel;

        initView();
        initLoadMore();
        getRoutingHistory();
        return v;
    }
    public void getRoutingHistory(){
        routingHistoryVM.onRefresh(fromDate, toDate, this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getHistorySuccess":
                adapter.getLoadMoreModule().loadMoreComplete();
                adapter.notifyDataSetChanged();
                if(routingHistoryVM.getRoutingPlans().size()>0){
                    routingHistoryVM.getIsNotData().set(false);
                }else{
                    routingHistoryVM.getIsNotData().set(true);
                }
                break;
            case "noMore":
                adapter.getLoadMoreModule().loadMoreEnd();
                break;
        }
    }

    private void initView() {
        initToolbar();

        adapter = new BaseAdapterV3(R.layout.item_routing_history, routingHistoryVM.getRoutingPlans(), this);
        mBinding.rcHistory.setAdapter(adapter);
        mBinding.swipeRefresh.setOnRefreshListener(() -> {
            routingHistoryVM.onRefresh(fromDate, toDate, this::runUi);
            adapter.notifyDataSetChanged();
        });

        // get start of the month
        Calendar cal = Calendar.getInstance();
        toDate = cal.getTime();
        mBinding.txtToDate.setText(format.format(toDate.getTime()));

        cal.set(Calendar.DAY_OF_MONTH, 1);
        fromDate = cal.getTime();
        mBinding.txtFromDate.setText(format.format(fromDate.getTime()));


        //set listener
        mBinding.txtFromDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                selectDate(v, fromDate, toDate);
            }
        });
        mBinding.txtToDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                selectDate(v, fromDate, toDate);
            }
        });
        mBinding.btnSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

            }
        });

    }

    @Override
    public void onItemClick(View v, Object o) {
        if(v.getId() == R.id.itemRouting){
            ShareVanRoutingPlan routingPlan = (ShareVanRoutingPlan) o;
            Intent intent = new Intent("com.ts.sharevandriver.TARGET_ROUTING_PLAN_DAY");
            intent.putExtra(Constants.ITEM_ID, routingPlan.getRouting_plan_day_code());
            intent.putExtra(Constants.FROM_FRAGMENT, "ROUTING_HISTORY_FRAGMENT");
            startActivity(intent);
        }
    }

    private void initToolbar() {
        mBinding.toolbar.setTitle(R.string.routing_history);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            routingHistoryVM.getMoreHistory(fromDate, toDate, this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void selectDate(View v, Date fromDate, Date toDate) {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            switch (v.getId()) {
                case R.id.txtFromDate:
                    c.setTime(fromDate);
                    break;
                case R.id.txtToDate:
                    c.setTime(toDate);
                    break;
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year1, month1, dayOfMonth);

                String date = format.format(calendar.getTime());

                switch (v.getId()) {
                    case R.id.txtFromDate:
                        ((TextView) v).setText(date);
                        //getData
                        fromDate.setTime(calendar.getTime().getTime());
                        getRoutingHistory();
                        break;
                    case R.id.txtToDate:
                        //getData
                        toDate.setTime(calendar.getTime().getTime());
                        ((TextView) v).setText(date);
                        getRoutingHistory();
                        break;
                }

            }, year, month, day);
            if (v.getId() == R.id.txtToDate) {
                datePickerDialog.getDatePicker().setMinDate(fromDate.getTime());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            } else if (v.getId() == R.id.txtFromDate) {
                datePickerDialog.getDatePicker().setMaxDate(toDate.getTime());
            }

            datePickerDialog.show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.routing_history_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingHistoryVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
