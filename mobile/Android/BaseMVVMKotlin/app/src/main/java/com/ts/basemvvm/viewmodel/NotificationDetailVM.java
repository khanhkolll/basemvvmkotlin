package com.ts.basemvvm.viewmodel;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.ts.basemvvm.api.NotificationAPI;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.model.NotificationModel;
import com.tsolution.base.BaseViewModel;

public class NotificationDetailVM extends BaseViewModel<NotificationModel> {
    public ObservableBoolean isLoading = new ObservableBoolean();


    public NotificationDetailVM(@NonNull Application application) {
        super(application);
    }

    public void getNotificationById(String notificationId){
        isLoading.set(true);
        NotificationAPI.getNotificationById(notificationId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                new Handler().postDelayed(()->{
                    isLoading.set(false);
                }, 500);
                model.set((NotificationModel) o);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }

}
