package com.ts.basemvvm.model;

import android.annotation.SuppressLint;
import android.widget.TextView;

import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ts.basemvvm.R;
import com.ts.basemvvm.base.AppController;
import com.tsolution.base.BaseModel;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver extends BaseModel {
    private Long id;
    private String driver_code;
    private String name;
    private String display_name;
    private String full_name;
    private String fleet_management_name;
    private String address;
    private String phone;
    private String tz;
    private String lang;
    private Integer nationality;
    private String user_driver;
    private String ssn;
    private OdooDate birth_date;
    private OdooDate expires_date;
    private OdooDate hire_date;
    private OdooDate leave_date;
    private Float average_rating;
    private Integer point;
    private String image_1920;
    private String gender;

    private String class_driver;
    private String no;
    private OdooDate driver_license_date;

    private AssignationLog assignation_log;
    private Vehicle vehicle;

    private Company company;


    public String getHire_date_str() {
        if (this.hire_date != null) {
            return AppController.formatDate.format(this.hire_date);
        }
        return "";
    }

    public String getBirth_date_str() {
        if (this.birth_date != null) {
            return AppController.formatDate.format(this.birth_date);
        }
        return "";
    }

    public String getExpires_date_str() {
        if (this.birth_date != null) {
            return AppController.formatDate.format(this.expires_date);
        }
        return "";
    }

    @BindingAdapter("gender")
    public static void genderStr(TextView textView, String gender) {
        if (gender == null) return;
        switch (gender) {
            case "male":
                textView.setText(R.string.male);
                break;
            case "female":
                textView.setText(R.string.female);
                break;
            case "other":
                textView.setText(R.string.other);
                break;
        }
    }

    @SuppressLint("DefaultLocale")
    public String getAverage_rating_str() {
        if (this.average_rating != null) {
            return String.format("%.1f", average_rating);
        }
        return "";
    }

}