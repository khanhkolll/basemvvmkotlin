package com.ts.basemvvm.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.XBaseAdapter;
import com.ts.basemvvm.databinding.FragmentIncentivePackingBinding;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListDialogFragment extends DialogFragment {
    private List lstData;
    private int itemLayout;
    private AdapterListener listener;
    private int title;
    private String description;

    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, List lstData, AdapterListener adapterListener) {
        this.lstData = lstData;
        this.itemLayout = itemLayout;
        this.listener = adapterListener;
        this.title = title;
    }
    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, String description, List lstData, AdapterListener adapterListener) {
        this.lstData = lstData;
        this.itemLayout = itemLayout;
        this.listener = adapterListener;
        this.title = title;
        this.description = description;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentIncentivePackingBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_incentive_packing, container, false);
        binding.txtTitle.setText(getString(title));

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        if(description != null){
            binding.txtMsg.setText(description);
        }else {
            binding.txtMsg.setVisibility(View.GONE);
        }

        XBaseAdapter packingAdapter = new XBaseAdapter(itemLayout, lstData, listener);
        RecyclerView rcIncentive = binding.rcIncentiveProduct;
        rcIncentive.setAdapter(packingAdapter);
        rcIncentive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());

        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

}