package com.ts.basemvvm.ui.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ts.basemvvm.R;
import com.ts.basemvvm.base.StaticData;
import com.ts.basemvvm.databinding.DialogRoutingVehicleBinding;
import com.ts.basemvvm.model.ParkingPoint;
import com.ts.basemvvm.ui.activity.VansInfoActivity;
import com.ts.basemvvm.widget.OnSingleClickListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

public class VansInfoDialog extends DialogFragment {
    public static int REQUEST_VAN_INFO = 111;
    DialogRoutingVehicleBinding binding;
    OnCheckLocationVans checkLocationVans;

    public VansInfoDialog(OnCheckLocationVans checkLocationVans) {
        this.checkLocationVans = checkLocationVans;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_routing_vehicle, container, false);
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());

        binding.txtParkingPlace.setOnClickListener(v -> {
            if (StaticData.getDriver().getVehicle().getParking_point() == null) {
                Toast.makeText(getContext(), R.string.can_not_get_parking_point, Toast.LENGTH_SHORT).show();
            } else {
                checkLocationVans.checkLocation(StaticData.getDriver().getVehicle().getParking_point());
                this.dismiss();
            }
        });


        binding.btnCancel.setOnClickListener(v -> this.dismiss());

        binding.btnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                openVansInfo();
                dismissDialog();
            }
        });
//        binding.btnConfirm.setOnClickListener(v -> {
//            openVansInfo();
//            this.dismiss();
//        });
        return binding.getRoot();
    }

    private void dismissDialog() {
        this.dismiss();
    }

    private void openVansInfo() {
        Intent intent = new Intent(requireActivity(), VansInfoActivity.class);
        requireActivity().startActivityForResult(intent, REQUEST_VAN_INFO);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }


    public interface OnCheckLocationVans {
        void checkLocation(ParkingPoint parkingPoint);
    }



}
