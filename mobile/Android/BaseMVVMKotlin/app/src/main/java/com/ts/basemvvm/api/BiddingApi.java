package com.ts.basemvvm.api;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.basemvvm.base.IResponse;
import com.ts.basemvvm.model.BiddingInformation;
import com.ts.basemvvm.model.ConfirmBiddingResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class BiddingApi extends BaseApi {
    public static void getListBiddingOrderShipping(String txt_search, int offset, int status, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("offset", offset);
            params.put("limit", 10);
            params.put("status", status);
            params.put("order_by", 0);
            params.put("txt_search", txt_search);

            mOdoo.callRoute("/bidding/get_list_bidding_order_shipping_v2", params, new SharingOdooResponse<OdooResultDto<BiddingInformation>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingInformation> resultDto) {
                    if (resultDto != null && resultDto.getRecords() != null) {
                        response.onSuccess(resultDto.getRecords());
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getOrderDetail(int biddingId, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        try {
            params.put("bidding_order_id", biddingId);

            mOdoo.callRoute("/bidding/get_bidding_order_detail_by_id", params, new SharingOdooResponse<OdooResultDto<BiddingInformation>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingInformation> resultDto) {
                    if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                        response.onSuccess(resultDto.getRecords().get(0));
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmBidding(String bidding_order_id, List<Integer> cargo_ids, String type, String confirm_time, IResponse response) {
        JSONObject params;
        params = new JSONObject();

        StringBuilder stringBuilder = new StringBuilder();
        String cargoIds = "";
        for (Integer id : cargo_ids) {
            stringBuilder.append(id).append(", ");
        }
        if (!stringBuilder.toString().isEmpty()) {
            cargoIds = stringBuilder.substring(0, stringBuilder.length() - 2);
        }

        try {
            params.put("bidding_order_id", bidding_order_id);
            params.put("cargo_ids", cargoIds);
            params.put("type", type);
            params.put("confirm_time", confirm_time);
            mOdoo.callRoute("/bidding/driver_confirm_cargo_quantity", params, new SharingOdooResponse<OdooResultDto<ConfirmBiddingResponse>>() {
                @Override
                public void onSuccess(OdooResultDto<ConfirmBiddingResponse> biddingResponse) {
                    response.onSuccess(biddingResponse);
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
