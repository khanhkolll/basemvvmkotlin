package com.ts.basemvvm.base;

import com.ts.basemvvm.model.ApiResponseModel;
import com.ts.basemvvm.model.SocketId;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SOSocketService {
    @POST("/setSocketId")
    Call<ApiResponseModel> setSocketId(
            @Header("Authorization") String cookie,
            @Body SocketId socketId);
}
