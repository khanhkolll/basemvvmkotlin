package com.ts.basemvvm.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.google.android.material.tabs.TabLayout;
import com.ts.basemvvm.R;
import com.ts.basemvvm.adapter.PagerAdapter;
import com.ts.basemvvm.base.Constants;
import com.ts.basemvvm.databinding.BiddingFragmentBinding;
import com.ts.basemvvm.enums.EnumBidding;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class BiddingFragment extends BaseFragment {
    private BiddingFragmentBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (BiddingFragmentBinding) binding;

        initViewPagerTabLayout();
        return v;
    }



    @Override
    public int getLayoutRes() {
        return R.layout.bidding_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private void initViewPagerTabLayout() {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.NOT_SHIPPING));
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.SHIPPING));
        adapter.addFragment(createFragment(EnumBidding.EnumBiddingBehavior.SHIPPING_DONE));
        mBinding.viewPager.setAdapter(adapter);
        mBinding.viewPager.setOffscreenPageLimit(4);
        mBinding.viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mBinding.tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private Fragment createFragment(int pos) {
        ShippingBillFragment biddingFragment = new ShippingBillFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_PAGE, pos);
        biddingFragment.setArguments(bundle);
        return biddingFragment;

    }
}
